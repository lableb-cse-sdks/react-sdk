# Lableb Provider

Lableb provider gives [Lableb](https://lableb.com)'s components the right context to your platform's data, with the capability to have more than one provider in the same app.

-------------

## usage

```js
<LablebProvider
    platformOptions={{
        platformName: 'PLATFORM_NAME',
        APIKey: 'API_KEY'
    }}
>
    <LablebSearch />
</LablebProvider>
```


--------------

## Props

| prop name       | type     | description |
| --------------- | -------- | ----------- |
| platformOptions | object | described below in details |
| layoutDirection | string | 'RTL' or 'LTR' specify all Lableb components layout direction no matter what is the language of the content |


--------------


## platform options

Platform options are globally scoped to the provider, when passing a specific value, that value get used in all "scoped" components unless you decide to override it in some inner component

--------------


## Example of scoped providers

```js

<LablebProvider
    platformOptions={{
        platformName: 'PLATFORM_NAME_1',
        APIKey: 'API_KEY_1'
    }}
>
    <LablebSearch />
</LablebProvider>


<LablebProvider
    platformOptions={{
        platformName: 'PLATFORM_NAME_2',
        APIKey: 'API_KEY_2'
    }}
>
    <LablebSearch />
</LablebProvider>

```


Inside the one single application we can use many instances of Lableb's search bar, each with different context, wrapping it in different `LablebProvider` and passing it different configuration about the platform itself.



------------------


## Example of platform options override

```js
<LablebProvider
    platformOptions={{
        platformName: 'PLATFORM_NAME',
        APIKey: 'API_KEY',
        indexName: 'posts',
    }}
>
    <LablebSearch
        searchOptions={{
            indexName: 'products',
        }}
    />
</LablebProvider>
```


Normally `LablebSearch` Component will use `posts` as parameters in its requests, but because we provide it with a custom value of `indexName`, now `LablebSearch` when doing the search request will override the globally scoped option and use the value `products` instead

---------------

### When values override can be useful?

In case your platform use many indexes to store the documents, one for each language, or one for each type of documents(blogs, products), you can pass the search options values that you need.

> Note: Clean approach to override
>
> Sometime override options can be easy, overriding one simple option value, but in some situation like overriding the `indexName` it might cause logical errors having the search bar save the results of the latest search request made to Lableb, so to avoid such things we recommend using different context (via different `LablebProvider` as described in the above section "Example of scoped providers")



----------------



## Platform options properties

| property              | type   | default   | description |
| --------------------- | -------| --------- | --------------- |
| platformName*         | string | -         | Your platform name as written at [Lableb Dashboard](https://dashboard.lableb.com) |
| APIKey*               | string | -         | Search, Autocomplete, Recommend, and Feedback API Key |
| indexName             | string | index     | The used data index name as created at [Lableb Dashboard \| Collections](https://dashboard.lableb.com) |
| indexingAPIKey        | string | -         | Index, Delete API Key |
| jwtToken              | string | -         | you can use it instead of any API Key |
| searchHandler         | string | default   | The used search handler name as found at [Lableb Dashboard \| Collections \| Handlers](https://dashboard.lableb.com) |
| autocompleteHandler   | string | suggest   | The used autocomplete handler name as found at [Lableb Dashboard \| Collections \| Handlers](https://dashboard.lableb.com) |
| recommendHandler      | string | recommend | The used recommend handler name as found at [Lableb Dashboard \| Collections \| Handlers](https://dashboard.lableb.com) |
| interceptors          | array of functions | [] | pass an array of interceptors functions to manipulate the request before being sent to Lableb. [Read in details](https://gitlab.com/lableb-cse-sdks/javascript-sdk/-/blob/master/documentation/interceptors.md) |
| userId                | string | -         | end user id |
| userIp                | string | -         | end user IP address |
| userCountry           | string | -         | end user country code(ISO-3166) |
| sessionId             | string | -         | uniques session Id for your end user |
| sessionIdGenerator    | function | -       | pass callback that generate unique session id string |
