# Lableb Search

Search components render a search box with autocomplete and filters(optional), search and autocomplete results are fetched automatically after some delay.

You can controls what the search bar contains, or render your own UI with render props as a children of the component.


## usage

```js
<LablebProvider
    platformOptions={{
        platformName: 'PLATFORM_NAME',
        APIKey: 'API_KEY'
    }}
>
    <LablebSearch />
</LablebProvider>
```

## props

| props name          | type      | description  |
| ------------------- | --------- | ------------ |
| searchOptions       | object    | override the global options passed to `LablebProvider`(details are below) |
| autocompleteOptions | object    | override the global options passed to `LablebProvider`(details are below) |
| inlineFilters       | boolean   | display embedded filters under the search bar |
| inlineLogo          | boolean   | display Lableb logo inside the search bar |
| preventAutoSearch   | boolean   | prevent auto search after some delay |
| children            | function  | render props function, used to render your own search box  |


### Search Options

| field               | type     | description |
| ------------------- | -------- | ----------- |                                                                 
| platformName        | string   | your platform name in small-letters |
| indexName           | string   | the used index name for the search function |
| APIKey              | string   | your search API Key copied from [Lableb Dashboard](https://dashboard.lableb.com) |
| jwtToken            | string   | the jwt token, in case you used the Login API to get it, where you can use it instead of any API Key |
| filter              | string   | add filters to your search request with a string syntax |
| sort                | string   | sort your results by specific field and specific order |
| limit               | number   | limit your search results to some number |
| skip                | number   | used for paginated data |
| userId              | string   | end user id |
| userIp              | string   | end user IP address |
| userCountry         | string   | end user country code(ISO-3166) |
| sessionId           | string   | uniques session Id for your end user |
| sessionIdGenerator  | function | pass callback that generate unique session id string |


### Autocomplete Options

| field               | type     | description |
| ------------------- | -------- | ----------- |    
| platformName        | string   | your platform name in small-letters |
| indexName           | string   | the used index name for the autocomplete function |
| APIKey              | string   | your autocomplete API Key copied from [Lableb Dashboard](https://dashboard.lableb.com) |
| jwtToken            | string   | the jwt token, in case you used the Login API to get it, where you can use it instead of any API Key |
| filter              | string   | add filters to your autocomplete request with a string syntax |
| sort                | string   | sort your results by specific field and specific order |
| limit               | number   | limit your autocomplete results to some number |
| userId              | string   | end user id |
| userIp              | string   | end user IP address |
| userCountry         | string   | end user country code(ISO-3166) |
| sessionId           | string   | uniques session Id for your end user |
| sessionIdGenerator  | function | pass callback that generate unique session id string |


### Children Render Props

You can use it to display a different UI based on your needs.

| prop name                  | type               | description | 
| -------------------------- | ------------------ | ----------- |
| query                      | string             | current query string |
| setQuery                   | function           | take the new value of user query |
| autocompleteResults        | array of documents | array of autocomplete results |
| openAutocompleteResults    | boolean            | open state of autocomplete results |
| setOpenAutocompleteResults | function           | set the open state of autocomplete results |
| sendAutocompleteFeedback   | function           | send autocomplete feedback manually based on user interaction |
| searchAtLableb             | function           | start a search request at Lableb |
| loading                    | boolean            | pending state of Lableb's requests |


### Example of render props

```js
<LablebProvider
    platformOptions={{
        platformName: 'PLATFORM_NAME',
        APIKey: 'API_KEY'
    }}
>
    <LablebSearch
        preventAutoSearch
    >
        {
            ({
                query,
                setQuery,
                autocompleteResults,
                openAutocompleteResults,
                setOpenAutocompleteResults,
                sendAutocompleteFeedback,
                searchAtLableb,
                loading
            }) => (
                <div>

                    <CustomersSearchBar
                        onEnter={() => {
                            searchAtLableb();
                            setOpenAutocompleteResults(false)
                        }}
                        onSubmit={() => {
                            searchAtLableb();
                            setOpenAutocompleteResults(false)
                        }}
                        value={query}
                        setValue={setQuery}
                        onFocus={function _onFocus() {
                            console.log('on focus')
                            setOpenAutocompleteResults(true);
                        }}
                        onBlur={function _onBlur() {
                            setTimeout(() => {
                                setOpenAutocompleteResults(false);
                            }, 200);
                        }}
                        placeholder={'DEMO_INPUT_PLACEHOLDER'}
                    />



                    {
                        (openAutocompleteResults && autocompleteResults.length) ?
                            <div>
                                {
                                    autocompleteResults
                                        ?.map((autocompleteResult, index) => (
                                            <AutocompleteResult
                                                key={`${autocompleteResult?.id}-${index}`}
                                                onClick={function _autocompleteResultClickHandler(autocompleteResult: LablebDocumentWithFeedback) {

                                                    setQuery(autocompleteResult?.phrase);
                                                    setOpenAutocompleteResults(false);

                                                    if (autocompleteResult.id)
                                                        sendAutocompleteFeedback(autocompleteResult)
                                                }}
                                                autocompleteResult={autocompleteResult}
                                                language={'en'}
                                            />
                                        ))
                                }
                            </div>
                            :
                            null
                    }

                </div>
            )
        }
    </LablebSearch>
</LablebProvider>
```