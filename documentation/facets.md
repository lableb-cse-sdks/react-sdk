# Facets Anatomy

Facets if an object that contains all available filters for future search request with a `count` property that shows how many filter keyword you can use
```json
{
  "count": 1,
  "categories": { ... }
}
```

Each filter property is in itself an object that contains the `buckets` array, which contains the possible values for that filter

```json
{
  "count": 1,
  "categories": {
    "buckets": [
      { value: "Accessories", ... },
      { value: "Hats", ... },
    ]
  }
}
```

And finally, each object inside the buckets array has a `value` and a `count` property that represents the documents count that exist for that filter value


```json
{
  "count": 1,
  "categories": {
    "buckets": [
      { value: "Accessories", value: 24 },
      { value: "Hats", value: 114 },
    ]
  }
}
```

Hope these details were clear for you, we'll share a real live platform facets response from one of our favorites platforms that has integrated with [Lableb](https://lableb.com)


```json
{
    "count": 5,
    "language": {
        "buckets": [
            {
                "value": "ar",
                "count": 199
            },
            {
                "value": "en",
                "count": 199
            }
        ]
    },
    "currency": {
        "buckets": []
    },
    "categories": {
        "buckets": [
            {
                "value": "Accessories",
                "count": 37
            },
            {
                "value": "أكسسوارات ",
                "count": 37
            },
            {
                "value": "Smart Home",
                "count": 28
            },
            {
                "value": "المنزل الذكي",
                "count": 28
            },
            {
                "value": " كيابل ",
                "count": 24
            },
            {
                "value": "Cable",
                "count": 24
            },
            {
                "value": "Other",
                "count": 24
            },
            {
                "value": "أخرى",
                "count": 24
            },
            {
                "value": "Adapter",
                "count": 21
            },
            {
                "value": "محولات طاقة  ",
                "count": 21
            },
            {
                "value": "Phone Accessories",
                "count": 20
            },
            {
                "value": "ملحقات الهاتف",
                "count": 20
            },
            {
                "value": "Power Bank",
                "count": 16
            },
            {
                "value": "بطاريات محمولة ",
                "count": 16
            },
            {
                "value": "Routers",
                "count": 11
            },
            {
                "value": "راوترات",
                "count": 11
            },
            {
                "value": "Cameras",
                "count": 6
            },
            {
                "value": "Receivers",
                "count": 6
            },
            {
                "value": "electronics",
                "count": 6
            },
            {
                "value": "الكترونيات",
                "count": 6
            }
        ]
    },
    "manufacturer": {
        "buckets": []
    },
    "status": {
        "buckets": [
            {
                "value": "Out Of Stock",
                "count": 194
            },
            {
                "value": "غير متوفر",
                "count": 194
            },
            {
                "value": "In Stock",
                "count": 5
            },
            {
                "value": "متوفر",
                "count": 5
            }
        ]
    }
}
```