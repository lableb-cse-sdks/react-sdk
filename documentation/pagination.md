# Lableb Pagination

You can use `LablebPagination` component to easily paginate your search results

## Props
| prop name | type     | description  |
| --------- | -------- | ------------ |
| startPage | number   | page number to start with |
| pageSize  | number   | returned documents counts |
| children  | function | render props function (described below) |


## Example

```js
<LablebProvider
    platformOptions={{
        APIKey: process.env.API_KEY,
        platformName: process.env.PLATFORM_NAME,
    }}
>
    
    <LablebSearch />

    <LablebPagination pageSize={9} />

</LablebProvider>
```


## Children

Taking the right state and props from Lableb Pagination helps you easily build any UI related to pagination of your own make.


### Render props

| prop name             | type             | description  |
| --------------------- | ---------------- | ------------ | 
| currentPage           | number           | current page of search results                     
| hasNextPage           | boolean          | `true` when there's still documents after the `currentPage`
| hasPrevPage           | boolean          | `true` when there's still documents before the `currentPage`      
| hasNextTenPages       | boolean          | `true` when there's still 10 pages of documents after the `currentPage`     
| hasPrevTenPages       | boolean          | `true` when there's still 10 pages of documents before the `currentPage`           
| onNextPage            | function         | fetch the next page of documents        
| onPrevPage            | function         | fetch the previous page of documents        
| onNextTenPages        | function         | fetch the next ten pages of documents        
| onPrevTenPages        | function         | fetch the previous ten pages of documents        
| pagesCount            | number           | pages count depends on your page size and documents count
| pagesNumbersToDisplay | array of numbers | give you the array of numbers you can display in your component, makes the current page the center of this array
| setPageNumber         | function         | change the page number, and fetch the wanted page of documents

### Example of render props

```js
<LablebProvider
    platformOptions={{
        APIKey: process.env.API_KEY,
        platformName: process.env.PLATFORM_NAME,
    }}
>
    <LablebSearch />
    <LablebPagination>
        {
            ({
                currentPage,
                hasNextPage,
                hasNextTenPages,
                hasPrevPage,
                hasPrevTenPages,
                onNextPage,
                onNextTenPages,
                onPrevPage,
                onPrevTenPages,
                pagesCount,
                pagesNumbersToDisplay,
                setPageNumber
            }) => (
                <>

                    {
                        hasPrevTenPages ?
                            <button
                                onClick={onPrevTenPages}
                            >
                                {'prev 10 pages'}
                            </button>
                            :
                            null
                    }


                    {
                        hasPrevPage ?
                            <button
                                onClick={onPrevPage}>
                                {'prev page'}
                            </button>
                            :
                            null
                    }


                    {
                        pagesNumbersToDisplay
                            .map(page =>
                                <button
                                    key={page}
                                    style={{ color: page == currentPage ? 'red' : 'black' }}
                                    onClick={() => setPageNumber(page)}
                                >
                                    {page + 1}
                                </button>)
                    }



                    {
                        hasNextPage ?
                            <button
                                onClick={onNextPage}>
                                {'next page'}
                            </button>
                            :
                            null
                    }

                    {
                        hasNextTenPages ?
                            <button
                                onClick={onNextTenPages}>
                                {'next ten pages'}
                            </button>
                            :
                            null
                    }
                </>
            )
        }
    </LablebPagination>
</LablebProvider>
```