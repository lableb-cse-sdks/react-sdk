# Lableb Search Filters

Display a complete list of search filters UI with different display options

## usage

```js
<LablebProvider
    platformOptions={{
        platformName: 'PLATFORM_NAME',
        APIKey: 'API_KEY',
    }}
>

    <LablebSearchFilters />

</LablebProvider>
```

## Props

| field                      | type           | description |
| -------------------------- | -------------- | ----------- |
| staticFacets               | SelectedFacets | fixed set of facets that is sent with every request |
| defaultFacets              | SelectedFacets | default set of facets that is initially set at start |
| useCheckbox                | boolean        | use checkboxes for filters or fallback to normal tags |
| facetsCount                | number         | number of facets names displayed before the show more button |
| singleColumn               | boolean        | align all filters in one single column |
| includeCount               | boolean        | include the number of documents for each filter value |
| useButtonForMoreBuckets    | boolean        | use normal button instead of icon for show more/show less filters |
| children                   | function       | render props function, enable you to render your own UI |


## Render props a custom UI

### Props 

| field          | type     | description |
| -------------- | -------- | ----------- |
| facets         | object   | search facets objects ([Read More In Facets.md](./facets.md)) |
| selectedFacets | object   | user selected facets |
| toggleFacet    | function | toggle a facet callback |
| resetFacets    | function | reset selected facets callback |


> Note: User selected facets structure
```json
"selectedFacets": {
    "facetName": ["facetValue1", "facetValue2", ...],
    ...
}
```


### Example

```js
<LablebProvider
    platformOptions={{
        platformName: 'PLATFORM_NAME',
        APIKey: 'API_KEY',
    }}
>

    <LablebSearchFilters>
        {
            ({
                facets,
                resetFacets,
                selectedFacets,
                toggleFacet,
            }) => (
                <>

                    <div>
                        <h1>{'Filters'}</h1>
                        <button onClick={resetFacets}>{'reset filters'}</button>
                    </div>

                    <div>
                        {
                            Object.keys(facets)
                                .map(facetName => (
                                    <div>
                                        <p>{facetName}</p>
                                        <div>
                                            {
                                                facets[facetName].buckets.map((bucket, index) => (
                                                    <div key={`${bucket.value}-${index}`}>
                                                        <label htmlFor={`${bucket.value}-${index}`}>
                                                            {bucket.value}
                                                        </label>
                                                        <input
                                                            type="checkbox"
                                                            checked={selectedFacets[facetName].includes(bucket.value)}
                                                            onChange={(event) => toggleFacet({ facetName, facetValue: bucket.value })}
                                                        />
                                                    </div>
                                                ))
                                            }
                                        </div>
                                    </div>
                                ))
                        }
                    </div>

                </>
            )
        }
    </LablebSearchFilters>

</LablebProvider>
```


## Virtual Search Filters

You can use the `VirtualSearchFilters` component that is the same as `LablebSearchFilters` but it's hidden and won't be displayed in the DOM tree, can be useful for certain cases such as using static facets to get special search results in some pages.