# Lableb Search Results

Display search results for the end users as cards, with the option to show related documentations or render your own UI with render props patter.

## usage

`LablebSearchResults` automatically display all search results for the current search query entered by the end user

```js
<LablebProvider
    platformOptions={{
        platformName: 'PLATFORM_NAME',
        APIKey: 'API_KEY',
    }}
>
    <LablebSearch />
    <LablebSearchResults />
</LablebProvider>
```

## Props

| prop name                | type      | description |
| ------------------------ | --------- | ----------- |
| openResultOnClick        | boolean   | open the result url on a separate page on click |
| redirectToResultOnClick  | boolean   | redirect to result url on click |
| onDocumentClick          | function  | on document click callback |
| showRelatedDocuments     | boolean   | show related documents in recommendation dialog |
| children                 | function  | use render props to render your own UI |

## Render props example

```js
<LablebProvider
    platformOptions={{
        platformName: 'PLATFORM_NAME',
        APIKey: 'API_KEY',
    }}
>
    <LablebSearch />
    <LablebSearchResults>
        {
            ({
                onSearchResultClick,
                searchResults,
            }) => (
                <>
                    {
                        searchResults.map(document => (
                            <div
                                key={document.id}
                                onClick={() => onSearchResultClick(document)}
                            >
                                <img
                                    src={document?.image}
                                    alt={document?.title}
                                />
                                <p>{document?.title}</p>
                                <p>{document?.description}</p>
                            </div>
                        ))
                    }
                </>
            )
        }
    </LablebSearchResults>
</LablebProvider>
```
