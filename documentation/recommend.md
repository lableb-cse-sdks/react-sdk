# Lableb Recommend

Display recommendations results of a given document

```js
<LablebProvider
    platformOptions={{
        platformName: 'PLATFORM_NAME',
        APIKey: 'API_KEY',
    }}
>

    <LablebSearch />

    <LablebRecommend
        id={3}
    />

</LablebProvider>
```

## Props 

| prop name        | type             | description |
| ---------------- | ---------------- | ----------- |  
| id               | string or number | document(data) id that you want similar document of |
| recommendOptions | object           | override global recommend options |
| children         | function         | render your custom UI for Lableb's recommendations |


### Recommend Options 

Recommend options override the global options passed to `LablebProvider`


| field               | type     | description |
| ------------------- | -------- | ----------- |                                          
| platformName        | string   | your platform name in small-letters |
| indexName           | string   | the used index name for the recommend function |
| APIKey              | string   | your recommend API Key copied from [Lableb Dashboard](https://dashboard.lableb.com) |
| jwtToken            | string   | the jwt token, in case you used the Login API to get it, where you can use it instead of any API Key |
| title               | string   | document(data) title that you want similar document of |
| url                 | string   | document(data) url that you want similar document of |
| filter              | string   | add filters to your recommend request with a string syntax |
| sort                | string   | sort your results by specific field and specific order |
| limit               | number   | limit your recommend results to some number |
| userId              | string   | end user id |
| userIp              | string   | end user IP address |
| userCountry         | string   | end user country code(ISO-3166) |
| sessionId           | string   | uniques session Id for your end user |
| sessionIdGenerator  | function | pass callback that generate unique session id string |


### Render Props 

Props passed to the render props function, that can help you build your own custom UI


| field                 | type      | description |
| --------------------- | --------- | ----------- |  
| recommendResults      | array     | lableb recommendations as an array of documents |
| sendRecommendFeedback | function  | manually send recommendation feedback to Lableb |
