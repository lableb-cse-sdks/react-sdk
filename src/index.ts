const packageJson = require('../package.json');

console.log(`Lableb React SDK VERSION ${packageJson.version}`);

export { LablebProvider } from './components/provider/provider';

export { LablebSearch } from './components/search';

export { LablebSearchResults } from './components/search-results';

export { LablebSearchFilters } from './components/search-filters';
export { VirtualSearchFilters } from './components/search-filters/search-filters.virtual';

export { LablebRecommend } from './components/recommend';

export { LablebPagination } from './components/pagination';


export { LablebDocumentWithFeedback } from '@lableb/javascript-sdk';
export { LablebDocumentWithRecommendFeedback } from '@lableb/javascript-sdk';

export { AutocompleteResult } from './ui-components/autocomplete-result/autocomplete-result';
