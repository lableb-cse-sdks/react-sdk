export const DEFAULT_SEARCH_RESULTS = {
    facets: {},
    found_documents: 0,
    results: []
}

export const DEFAULT_AUTOCOMPLETE_RESULTS = {
    facets: {},
    found_documents: 0,
    results: []
}
