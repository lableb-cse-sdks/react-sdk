import React, { useCallback, useContext, useState } from "react";
import { LablebDocumentWithFeedback } from "@lableb/javascript-sdk";
import { GlobalStateContext } from "../../state/state.context";
import { SearchResultCard } from "../../ui-components/search-result-card/search-result-card";
import { SearchResultsCardsContainer } from "../../ui-components/search-results-cards-container/search-results-cards-container";
import { SearchResultsProps, SearchResultsRenderProps } from "./search-results.type";
import { LablebDialog } from "../../ui-components/lableb-dialog/lableb-dialog";
import { LablebRecommend } from "../recommend";
import { translations } from "../../ui-components/search-box/search.translation";
import { useSearchResultsStyles } from "./search-results.style";



export function SearchResultsUI(props: SearchResultsProps) {

    const {
        children,
        openResultOnClick,
        redirectToResultOnClick,
        onDocumentClick,
        showRelatedDocuments,
    } = props;


    const classes = useSearchResultsStyles();
    const { state } = useContext(GlobalStateContext);
    const [selectedDocumentId, setSelectedDocumentId] = useState('');
    const { language } = state;
    const searchResults = state.searchResults.results;


    function documentClickHandler(this: LablebDocumentWithFeedback) {

        sendSearchFeedback(this);

        if (openResultOnClick || redirectToResultOnClick) {

            if (this.url) {
                if (openResultOnClick)
                    window.open(this.url, '_blank')?.focus();
                else if (redirectToResultOnClick)
                    window.open(this.url, '_self')?.focus();
            }

            else
                console.warn(`No URL with document ${this.id}`);
        }

        if (onDocumentClick)
            onDocumentClick(this);
    }


    const sendSearchFeedback = useCallback((document: LablebDocumentWithFeedback) => {
        state.client?.feedback.search.single({ documentFeedback: document.feedback });
    }, [state.client]);


    const childrenProps: SearchResultsRenderProps = {
        searchResults,
        onSearchResultClick: sendSearchFeedback
    }



    if (children)
        return (
            <>
                {children(childrenProps)}
            </>
        );




    return (
        <>

            {
                showRelatedDocuments ?
                    <LablebDialog
                        showAcceptButton={false}
                        header={(
                            <p className={classes.dialogHeadText}>
                                {translations[language]['RECOMMENDATIONS']}
                            </p>
                        )}
                        stretchContent
                        body={(
                            <LablebRecommend
                                recommendOptions={{ limit: 9 }}
                                id={selectedDocumentId} />
                        )}
                        open={Boolean(selectedDocumentId)}
                        onClose={() => setSelectedDocumentId('')}
                        language={language}
                    />
                    :
                    null
            }


            <SearchResultsCardsContainer>
                {
                    searchResults
                        .map((document, index) => (
                            <SearchResultCard
                                key={`${document.id}-${index}`}
                                onClick={documentClickHandler.bind(document)}
                                document={document}
                                language={state.language}
                                onRelatedClick={(document) => setSelectedDocumentId(document.id)}
                                hideRelatedButton={!showRelatedDocuments}
                            />
                        ))
                }
            </SearchResultsCardsContainer>

        </>
    );
}