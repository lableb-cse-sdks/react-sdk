import { ReactNode } from "react";
import { LablebClientSearchResponse, LablebDocumentWithFeedback } from "@lableb/javascript-sdk";



export interface SearchResultsRenderProps {

    /** search results of a search request at lableb */
    searchResults: LablebClientSearchResponse['results'],

    /** use it when the user interact with some document to auto send the feedback */
    onSearchResultClick: (document: LablebDocumentWithFeedback) => void,
}



export interface SearchResultsProps {

    /** open the result url on a separate page on click */
    openResultOnClick?: boolean,

    /** redirect to result url on click */
    redirectToResultOnClick?: boolean,

    /** on document click callback */
    onDocumentClick?: (document: LablebDocumentWithFeedback) => void,

    /** show related documents in recommendation dialog */
    showRelatedDocuments?: boolean,

    children?: (props: SearchResultsRenderProps) => ReactNode,
}