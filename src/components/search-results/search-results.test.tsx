import React from 'react';
import { LablebProvider } from '../provider';
import { render, fireEvent, waitFor, screen, act } from '@testing-library/react'
import '@testing-library/jest-dom';
import { LablebSearch } from '../search';
import { LablebSearchResults } from './search-results';


test('render search results', async () => {

    render(
        <LablebProvider
            platformOptions={{
                platformName: 'PLATFORM_NAME',
                APIKey: 'API_KEY',
            }}
        >
            <LablebSearch />
            <LablebSearchResults />
        </LablebProvider>
    );
});

test('render search results with render props', async () => {

    render(
        <LablebProvider
            platformOptions={{
                platformName: 'PLATFORM_NAME',
                APIKey: 'API_KEY',
            }}
        >
            <LablebSearch />
            <LablebSearchResults>
                {
                    ({
                        onSearchResultClick,
                        searchResults,
                    }) => (
                        <>
                            {
                                searchResults.map(document => (
                                    <div
                                        key={document.id}
                                        onClick={() => onSearchResultClick(document)}
                                    >
                                        <img
                                            src={document?.image}
                                            alt={document?.title}
                                        />
                                        <p>{document?.title}</p>
                                        <p>{document?.description}</p>
                                    </div>
                                ))
                            }
                        </>
                    )
                }
            </LablebSearchResults>
        </LablebProvider>
    );
});