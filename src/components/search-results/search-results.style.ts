import { createUseStyles } from "react-jss";

export const useSearchResultsStyles = createUseStyles({
  "dialogHeadText": {
    margin: "0",
    color: "var(--main-text-color)",
    fontSize: "1.2rem"
  }
});
