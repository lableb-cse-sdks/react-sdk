import React from "react";
import { SearchResultsErrorBoundary } from "./search-results.boundary";
import { SearchResultsUI } from "./search-results.ui";


export function LablebSearchResults(props: React.ComponentProps<typeof SearchResultsUI>) {

    return (
        <SearchResultsErrorBoundary>
            <SearchResultsUI {...props} />
        </SearchResultsErrorBoundary>
    );
}