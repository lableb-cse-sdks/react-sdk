import React, { useEffect, useReducer } from "react";
import { LablebProviderProps } from "./provider.type";
import { GlobalStateContext, GLOBAL_STATE_DEFAULT } from "../../state/state.context";
import { LablebClient } from "@lableb/javascript-sdk";
import { setPlatformOptions } from "../../actions/lableb-client/platform.options";
import { saveLablebClient } from "../../actions/lableb-client/lableb-client.save";
import { globalStateReducer } from "../../state/state.reducer";
import { setLanguage } from "../../actions/language/languet.set";
import clsx from "clsx";
import { useProviderStyles } from "./provider.style";


export function LablebProvider(props: LablebProviderProps) {

  const {style, children, platformOptions, layoutDirection } = props;
  const classes = useProviderStyles();

  const [state, dispatch] = useReducer(
    globalStateReducer,
    GLOBAL_STATE_DEFAULT
  );

  useEffect(function updatePlatformOptions() {

    dispatch(setPlatformOptions({ platformOptions: platformOptions || {} }));

    (async function _createClient() {
      dispatch(saveLablebClient({ lablebClient: await LablebClient(platformOptions || {}) }))
    })()

  }, [platformOptions]);


  useEffect(function updateLayoutDirection() {

    dispatch(setLanguage({ language: layoutDirection == 'RTL' ? 'ar' : 'en' }));

  }, [layoutDirection]);



  return (
    <GlobalStateContext.Provider value={{ state, dispatch }}>
      <div
        className={clsx(classes.lablebRoot, {
          [classes.lablebRootAr]: state.language == 'ar'
        })}
        style={style}>
        {children}
      </div>
    </GlobalStateContext.Provider>
  );
}
