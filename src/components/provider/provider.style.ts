import { createUseStyles } from "react-jss";

export const useProviderStyles = createUseStyles({
    '@global': {
        '@import': [
            "url('https://fonts.googleapis.com/css2?family=Poppins:wght@200;300;400;500;600;700&display=swap')",
            "url('https://fonts.googleapis.com/css2?family=Tajawal:wght@300;400;500;700;800&display=swap')",
        ],
        ":root": { "--main-color": "#345b7394", "--main-text-color": "#345b73" },
        "@keyframes fade-in-bck": {
            "0%": {
                WebkitTransform: "translateZ(80px)",
                transform: "translateZ(80px)",
                opacity: 0
            },
            "100%": {
                WebkitTransform: "translateZ(0)",
                transform: "translateZ(0)",
                opacity: 1
            }
        },
        "@keyframes fade-out-bck": {
            "0%": {
                WebkitTransform: "translateZ(0)",
                transform: "translateZ(0)",
                opacity: 1
            },
            "100%": {
                WebkitTransform: "translateZ(-80px)",
                transform: "translateZ(-80px)",
                opacity: 0
            }
        },
        "@keyframes fadeout": { "100%": { opacity: 0 } },
        "@keyframes getWidth": {
            "90%": { width: "calc(100% - 1px)" },
            "100%": { backgroundColor: "transparent" }
        },
        "@keyframes fadeIn": { "0%": { opacity: 0 }, "100%": { opacity: 1 } }
    },
    "lablebRoot": { fontFamily: "'Poppins', sans-serif " },
    "lablebRootAr": { fontFamily: "'Tajawal', sans-serif " }
});
