import React from 'react';
import { LablebProvider } from './provider';
import { render, fireEvent, waitFor, screen, act } from '@testing-library/react'
import '@testing-library/jest-dom';
import { LablebSearch } from '../search';


test('render provider', async () => {

    render(
        <LablebProvider
            platformOptions={{
                platformName: 'PLATFORM_NAME',
                APIKey: 'API_KEY'
            }}
            layoutDirection="RTL"
            
        >
            <LablebSearch
               
            />
        </LablebProvider>
    );
});
