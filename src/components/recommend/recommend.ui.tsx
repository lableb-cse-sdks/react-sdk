import React, { useCallback, useContext, useEffect, useState } from "react";
import { LablebClientRecommendResponse, LablebDocumentWithFeedback } from "@lableb/javascript-sdk";
import { LablebDocumentWithRecommendFeedback } from "@lableb/javascript-sdk";
import { GlobalStateContext } from "../../state/state.context";
import { SearchResultCard } from "../../ui-components/search-result-card/search-result-card";
import { SearchResultsCardsContainer } from "../../ui-components/search-results-cards-container/search-results-cards-container";
import { LablebRecommendProps, RecommendRenderProps } from "./recommend.type";


export function LablebRecommendUI(props: LablebRecommendProps) {

    const {
        id,
        recommendOptions,
        children,
    } = props;

    const { state } = useContext(GlobalStateContext);

    const [recommendResults, setRecommendResults] = useState<LablebClientRecommendResponse>({
        facets: {},
        found_documents: 0,
        results: []
    });


    useEffect(function fetchRecommendation() {

        ; (async function _fetchRecommendation() {

            if (id) {

                const recommendResponse = await state.client?.recommend({
                    id,
                    ...recommendOptions,
                });

                if (recommendResponse?.response)
                    setRecommendResults(recommendResponse.response)
            }

        })();

    }, [id, state.client, recommendOptions]);



    const sendRecommendFeedback = useCallback(
        function _sendRecommendFeedback(document: LablebDocumentWithRecommendFeedback | LablebDocumentWithFeedback) {

            state.client
                ?.feedback
                .recommend
                .single({
                    documentFeedback: (document as LablebDocumentWithRecommendFeedback).feedback
                })

        }, [state.client]);



    const childrenProps: RecommendRenderProps = {
        sendRecommendFeedback,
        recommendResults: recommendResults.results,
    }


    if (children)
        return (
            <>
                {children(childrenProps)}
            </>
        );


    return (
        <SearchResultsCardsContainer shrink>
            {
                recommendResults
                    .results
                    .map((document, index) => (
                        <SearchResultCard
                            shrink
                            onClick={sendRecommendFeedback}
                            key={`${document.id}-${index}`}
                            document={document}
                            language={state.language}
                            hideRelatedButton
                        />
                    ))
            }
        </SearchResultsCardsContainer>
    );
}