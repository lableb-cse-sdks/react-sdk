import { LablebClientRecommendResponse, LablebDocumentWithRecommendFeedback, LablebRecommendOptions } from "@lableb/javascript-sdk";
import { ReactNode } from "react";

export type OverrideRecommendOptions = Omit<LablebRecommendOptions, "recommendHandler" | "id">;

export interface RecommendRenderProps {

    /** lableb recommendations as an array of documents */
    recommendResults: LablebClientRecommendResponse['results'],

    /** manually send recommendation feedback to Lableb */
    sendRecommendFeedback: (document: LablebDocumentWithRecommendFeedback) => void,

}

export interface LablebRecommendProps {

    /** document(data) id that you want similar document of */
    id?: string | number,

    /** override global recommend options */
    recommendOptions?: OverrideRecommendOptions,

    /** render props your custom UI */
    children?: (props: RecommendRenderProps) => ReactNode,
}