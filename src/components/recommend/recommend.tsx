import React from "react";
import { LablebRecommendErrorBoundary } from "./recommend.boundary";
import { LablebRecommendUI } from "./recommend.ui";

export function LablebRecommend(props: React.ComponentProps<typeof LablebRecommendUI>) {

    return (
        <LablebRecommendErrorBoundary>
            <LablebRecommendUI {...props} />
        </LablebRecommendErrorBoundary>
    );
}