import React from 'react';


export class LablebRecommendErrorBoundary extends React.Component<any, any> {

    constructor(props: any) {
        super(props);
        this.state = { hasError: false };
    }

    static getDerivedStateFromError(error: any) {

        return { hasError: true };
    }

    componentDidCatch(error: any, errorInfo: any) {

        console.error(error);
        console.error(errorInfo);
        console.log('Contact support@lableb.com for further help');
    }

    render() {
        if (this.state.hasError) {
            return (
                <h1>{`Something went wrong with the search,check your console`}</h1>
            );
        }

        return this.props.children;
    }
}