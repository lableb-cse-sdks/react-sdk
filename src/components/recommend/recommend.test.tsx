import React from 'react';
import { LablebProvider } from '../provider';
import { render, fireEvent, waitFor, screen, act } from '@testing-library/react'
import '@testing-library/jest-dom';
import { LablebSearch } from '../search';
import { LablebRecommend } from './recommend';



test('render recommendations', async () => {

    render(
        <LablebProvider
            platformOptions={{
                platformName: 'PLATFORM_NAME',
                APIKey: 'API_KEY',
            }}
        >

            <LablebSearch />

            <LablebRecommend
                id={3}
            />

        </LablebProvider>
    );

})