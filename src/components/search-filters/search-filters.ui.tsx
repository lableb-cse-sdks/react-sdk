import React, { useCallback, useContext } from "react";
import { toggleFacet } from "../../actions/search/facets.toggle";
import { resetFacets } from "../../actions/search/facets.reset";
import { GlobalStateContext } from "../../state/state.context";
import { useDefaultFacets } from "./facets.hook";
import { mergeResponseFacetsWithInputFacets } from "./facets.utils";
import { LablebSearchFiltersProps, SearchFiltersRenderProps } from "./search-filters.type";
import { SearchFacets } from "../../ui-components/search-facets/search-facets";
import { translations } from "../../ui-components/search-box/search.translation";
import { useSearchFiltersStyles } from "./search-filters.style";




export function LablebSearchFiltersUI(props: LablebSearchFiltersProps) {


    const {
        staticFacets,
        defaultFacets,
        children,
        useCheckbox,
        facetsCount,
        singleColumn,
        includeCount,
        useButtonForMoreBuckets,
    } = props;
    
    const { state, dispatch } = useContext(GlobalStateContext);
    const selectedFacets = state.facets || {};
    const { language } = state;
    const classes = useSearchFiltersStyles();

    useDefaultFacets({ defaultFacets });

    const resetSelectedFacets = useCallback(function _resetSelectedFacets() {
        dispatch(resetFacets({}));
    }, []);


    /**
     * first append static facets(inner function call)
     * then merge user selected facets
     */
    const mergedFacets = mergeResponseFacetsWithInputFacets({
        responseFacets: mergeResponseFacetsWithInputFacets({
            responseFacets: state.searchResults.facets,
            inputFacets: staticFacets || {}
        }),
        inputFacets: selectedFacets
    });



    const toggleFacetsHandler = useCallback(({ facetValue, facetName }) => {
        dispatch(toggleFacet({ facetName, facetValue }));
    }, []);



    const childrenProps: SearchFiltersRenderProps = {
        facets: mergedFacets,
        selectedFacets: selectedFacets,
        toggleFacet: toggleFacetsHandler,
        resetFacets: resetSelectedFacets
    }


    if (children)
        return (
            <>
                {children(childrenProps)}
            </>
        );


    return (
        <>
            <div className={classes.searchFacetsContainer}>

                <div className={classes.searchFacetsHead}>

                    <p>
                        {translations[language]['FILTERS']}
                    </p>

                    <button onClick={resetSelectedFacets}>
                        {translations[language]['RESET']}
                    </button>

                </div>

                <SearchFacets
                    facets={mergedFacets}
                    selectedFacets={state.facets ?? {}}
                    toggleFacetsHandler={toggleFacetsHandler}
                    language={language}
                    useCheckbox={useCheckbox}
                    facetsCount={facetsCount}
                    singleColumn={singleColumn}
                    includeCount={includeCount}
                    useButtonForMoreBuckets={useButtonForMoreBuckets}
                />

            </div>
        </>
    );

}