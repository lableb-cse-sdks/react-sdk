import React from "react";
import { LablebSearchFiltersErrorBoundary } from "./search-filters.boundary";
import { LablebSearchFiltersUI } from "./search-filters.ui";

export function LablebSearchFilters(props: React.ComponentProps<typeof LablebSearchFiltersUI>) {

    return (
        <LablebSearchFiltersErrorBoundary>
            <LablebSearchFiltersUI {...props} />
        </LablebSearchFiltersErrorBoundary>
    );
}