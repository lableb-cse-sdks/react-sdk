import React from "react";

import { LablebSearchFilters } from './search-filters';

export function VirtualSearchFilters(props: React.ComponentProps<typeof LablebSearchFilters>) {

    return (
        <div style={{ display: 'none' }}>
            <LablebSearchFilters {...props} />
        </div>
    );
}