import { LablebClientSearchResponse, SelectedFacets } from "@lableb/javascript-sdk";
import produce from "immer";

function cleanFacets(facets: LablebClientSearchResponse['facets']): LablebClientSearchResponse['facets'] {

    return Object.keys(facets)
        .filter(facetName => facets[facetName])
        .filter(facetName => facets[facetName].buckets)
        .filter(facetName => facets[facetName].buckets.length)
        .map(facetName => ({
            [facetName]: {
                buckets: facets[facetName].buckets.filter(bucket => bucket.value != '' && bucket.count != 0)
            }
        }))
        .reduce(function mergeFacets(PrevFacets, currentFacet) {
            return {
                ...PrevFacets,
                ...currentFacet,
            }
        }, {});
}



export function mergeResponseFacetsWithInputFacets({
    inputFacets,
    responseFacets,
}: {
    inputFacets: SelectedFacets,
    responseFacets: LablebClientSearchResponse['facets']
}) {


    const mergedFacets = produce(responseFacets, function mergeFacets(facets) {

        if (inputFacets) {

            Object.keys(inputFacets)
                .forEach(staticFacetName => {

                    if (facets[staticFacetName]) {

                        const staticFacetsValues = inputFacets[staticFacetName];

                        staticFacetsValues
                            .forEach((staticFacetValue) => {

                                const facetValueExist = facets[staticFacetName].buckets.find(bucket => bucket.value == staticFacetValue);

                                if (facetValueExist) {
                                    // do nothing because it already exist
                                } else {
                                    facets[staticFacetName].buckets = facets[staticFacetName].buckets.concat({ value: staticFacetValue, count: 0 });
                                }

                            });

                    } else {

                        facets[staticFacetName] = { buckets: inputFacets[staticFacetName].map(facetValue => ({ value: facetValue, count: 0 })) }
                    }

                });
        }
    });




    return cleanFacets(mergedFacets);
}
