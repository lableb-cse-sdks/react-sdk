import React from 'react';
import { LablebProvider } from '../provider';
import { render, fireEvent, waitFor, screen, act } from '@testing-library/react'
import '@testing-library/jest-dom';
import { LablebSearchFilters } from './search-filters';


test('render search filters', async () => {

    render(
        <LablebProvider
            platformOptions={{
                platformName: 'PLATFORM_NAME',
                APIKey: 'API_KEY',
            }}
        >

            <LablebSearchFilters />

        </LablebProvider>
    );

});



test('render search filters with static facets', async () => {

    render(
        <LablebProvider
            platformOptions={{
                platformName: 'PLATFORM_NAME',
                APIKey: 'API_KEY',
            }}
        >

            <LablebSearchFilters
                staticFacets={{
                    'tags': ['sport']
                }}
            />

        </LablebProvider>
    );

});


test('render search filters with default facets', async () => {

    render(
        <LablebProvider
            platformOptions={{
                platformName: 'PLATFORM_NAME',
                APIKey: 'API_KEY',
            }}
        >

            <LablebSearchFilters
                defaultFacets={{
                    'tags': ['sport']
                }}
            />

        </LablebProvider>
    );

});

test('render search filters with custom UI', async () => {

    render(
        <LablebProvider
            platformOptions={{
                platformName: 'PLATFORM_NAME',
                APIKey: 'API_KEY',
            }}
        >

            <LablebSearchFilters>
                {
                    ({
                        facets,
                        resetFacets,
                        selectedFacets,
                        toggleFacet,
                    }) => (
                        <>

                            <div>
                                <h1>{'Filters'}</h1>
                                <button onClick={resetFacets}>{'reset filters'}</button>
                            </div>

                            <div>
                                {
                                    Object.keys(facets)
                                        .map(facetName => (
                                            <div>
                                                <p>{facetName}</p>
                                                <div>
                                                    {
                                                        facets[facetName].buckets.map((bucket, index) => (
                                                            <div key={`${bucket.value}-${index}`}>
                                                                <label htmlFor={`${bucket.value}-${index}`}>
                                                                    {bucket.value}
                                                                </label>
                                                                <input
                                                                    type="checkbox"
                                                                    checked={selectedFacets[facetName].includes(bucket.value)}
                                                                    onChange={(event) => toggleFacet({ facetName, facetValue: bucket.value })}
                                                                />
                                                            </div>
                                                        ))
                                                    }
                                                </div>
                                            </div>
                                        ))
                                }
                            </div>

                        </>
                    )
                }
            </LablebSearchFilters>

        </LablebProvider>
    );

});