import { LablebClientSearchResponse } from "@lableb/javascript-sdk";
import { SelectedFacets } from "@lableb/javascript-sdk";
import { ReactNode } from "react";

export interface SearchFiltersRenderProps {
    
    /** search facets */
    facets: LablebClientSearchResponse['facets'],
    
    /** user selected facets */
    selectedFacets: SelectedFacets,
    
    /** toggle a facet callback */
    toggleFacet: (params: { facetName: string, facetValue: string }) => void,
    
    /** reset selected facets */
    resetFacets: () => void,
}


export interface LablebSearchFiltersProps {

    /** fixed set of facets that is sent with every request */
    staticFacets?: SelectedFacets,

    /** default set of facets that is initially set at start */
    defaultFacets?: SelectedFacets,

    /** use checkboxes for filters or fallback to normal tags */
    useCheckbox?: boolean,

    /** number of facets names displayed before the show more button */
    facetsCount?: number,

    /** align all filters in one single column */
    singleColumn?: boolean,

    /** include the number of documents for each filter value */
    includeCount?: boolean,

    /** use normal button instead of icon for show more/show less filters */
    useButtonForMoreBuckets?: boolean,

    children?: (props: SearchFiltersRenderProps) => ReactNode,
}