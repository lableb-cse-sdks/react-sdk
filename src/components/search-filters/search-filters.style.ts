import { createUseStyles } from 'react-jss';

export const useSearchFiltersStyles = createUseStyles({
    "searchFacetsContainer": { padding: "15px" },
    "searchFacetsHead": {
        display: "flex",
        justifyContent: "space-between",
        alignItems: "center",
        "&>p": {
            color: "var(--main-text-color)",
            fontSize: "1.4rem"
        },
        "&>button": {
            fontSize: "12px",
            color: "var(--main-text-color)",
            border: "none",
            backgroundColor: "transparent",
            cursor: "pointer"
        }
    },

})