import { useContext, useEffect } from "react";
import { SelectedFacets } from "@lableb/javascript-sdk";
import { toggleFacet } from "../../actions/search/facets.toggle";
import { GlobalStateContext } from "../../state/state.context";

export function useDefaultFacets({ defaultFacets }: { defaultFacets?: SelectedFacets }) {

    const { dispatch } = useContext(GlobalStateContext);

    useEffect(function preSelectFacetsOnStart() {
        /**
         * for each default selected facet we toggle
         */
        if (defaultFacets) {
            Object
                .keys(defaultFacets)
                .forEach(facetName =>
                    defaultFacets[facetName]
                        .forEach(facetValue =>
                            dispatch(
                                toggleFacet({ facetName, facetValue })
                            )
                        )
                )
        }

    }, [defaultFacets]);

}