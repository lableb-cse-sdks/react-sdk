import React from 'react';


export class LablebPaginationErrorBoundary extends React.Component<any, any> {

    constructor(props: any) {
        super(props);
        this.state = { hasError: false };
    }

    static getDerivedStateFromError(error: any) {

        return { hasError: true };
    }

    componentDidCatch(error: any, errorInfo: any) {

        console.error(error);
        console.error(errorInfo);
        console.log('Contact support@lableb.com for further help');
    }

    render() {
        if (this.state.hasError) {
            return (
                <span style={{ color: 'red' }}>
                    {'Something wrong went in pagination component, please read the error in console'}
                </span>
            );
        }

        return this.props.children;
    }
}