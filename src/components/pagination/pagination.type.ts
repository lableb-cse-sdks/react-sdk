import { ReactNode } from "react";

export interface LablebPaginationUIProps {
    currentPage: number,
    hasNextPage: boolean,
    hasNextTenPages: boolean,
    hasPrevPage: boolean,
    hasPrevTenPages: boolean,
    pagesCount: number,
    onNextPage: () => void,
    onPrevPage: () => void,
    onNextTenPages: () => void,
    onPrevTenPages: () => void,
    pagesNumbersToDisplay: number[],
    setPageNumber: (pageNumber: number) => void,
}

export interface LablebPaginationProps {

    children?: (params: LablebPaginationUIProps) => ReactNode,

    pageSize?: number,
    startPage?: number,
}