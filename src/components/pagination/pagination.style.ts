import { createUseStyles } from "react-jss";

export const usePaginationStyle = createUseStyles({
    "container": {
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        padding: "10px"
    },
    "pageButton": {
        background: "transparent",
        border: "none",
        padding: "10px",
        borderRadius: "50%",
        width: "25px",
        height: "25px",
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        cursor: "pointer",
        fontSize: "0.8rem",
        margin: "0 2px"
    },
    "pageButton:hover": {
        background: "#e8e8e8"
    },
    "selectedPage": {
        backgroundColor: "var(--main-text-color)",
        color: "white"
    },
    "reverseImage": {
        transform: "scaleX(-1)"
    }
});