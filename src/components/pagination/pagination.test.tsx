import React from 'react';
import { LablebProvider } from '../provider';
import { LablebPagination } from './pagination';
import { render, fireEvent, waitFor, screen, act } from '@testing-library/react'
import '@testing-library/jest-dom';
import { LablebSearch } from '../search';



test('Render Pagination', async () => {

    await act(async () => render(
        <div data-testid="__test__container">
            <LablebProvider
                platformOptions={{
                    platformName: 'PLATFORM_NAME',
                    APIKey: 'API_KEY',
                }}
            >
                <LablebSearch />
                <LablebPagination />
            </LablebProvider>
        </div>
    ));



    await waitFor(() => screen.getByTestId('__test__container'))


    // await waitFor(() => expect(screen.queryByText('2')).toBeInTheDocument())
    // expect(screen.queryByText('1')).toBeInTheDocument();
    // expect(screen.queryByText('2')).toBeInTheDocument();
    // expect(screen.queryByText('3')).toBeInTheDocument();
    // expect(screen.queryByText('4')).toBeInTheDocument();
    // expect(screen.queryByText('5')).toBeInTheDocument();
});


test('Render Pagination with start page', async () => {

    await act(async () => render(
        <div data-testid="__test__container">
            <LablebProvider
                platformOptions={{
                    platformName: 'PLATFORM_NAME',
                    APIKey: 'API_KEY',
                }}
            >
                <LablebSearch />
                <LablebPagination
                    startPage={4}
                />
            </LablebProvider>
        </div>
    ));



    await waitFor(() => screen.getByTestId('__test__container'))


    // await waitFor(() => expect(screen.queryByText('2')).toBeInTheDocument())
    // expect(screen.queryByText('1')).toBeInTheDocument();
    // expect(screen.queryByText('2')).toBeInTheDocument();
    // expect(screen.queryByText('3')).toBeInTheDocument();
    // expect(screen.queryByText('4')).toBeInTheDocument();
    // expect(screen.queryByText('5')).toBeInTheDocument();
});


test('Render Pagination with page size', async () => {

    await act(async () => render(
        <div data-testid="__test__container">
            <LablebProvider
                platformOptions={{
                    platformName: 'PLATFORM_NAME',
                    APIKey: 'API_KEY',
                }}
            >
                <LablebSearch />
                <LablebPagination
                    pageSize={5}
                />
            </LablebProvider>
        </div>
    ));



    await waitFor(() => screen.getByTestId('__test__container'))


    // await waitFor(() => expect(screen.queryByText('2')).toBeInTheDocument())
    // expect(screen.queryByText('1')).toBeInTheDocument();
    // expect(screen.queryByText('2')).toBeInTheDocument();
    // expect(screen.queryByText('3')).toBeInTheDocument();
    // expect(screen.queryByText('4')).toBeInTheDocument();
    // expect(screen.queryByText('5')).toBeInTheDocument();
});



test('Render Pagination with render props', async () => {

    await act(async () => render(
        <div data-testid="__test__container">

            <LablebProvider
                platformOptions={{
                    platformName: 'PLATFORM_NAME',
                    APIKey: 'API_KEY',
                }}
            >
                <LablebSearch />
                <LablebPagination>
                    {
                        ({
                            currentPage,
                            hasNextPage,
                            hasNextTenPages,
                            hasPrevPage,
                            hasPrevTenPages,
                            onNextPage,
                            onNextTenPages,
                            onPrevPage,
                            onPrevTenPages,
                            pagesCount,
                            pagesNumbersToDisplay,
                            setPageNumber
                        }) => (
                            <>

                                {
                                    hasPrevTenPages ?
                                        <button
                                            onClick={onPrevTenPages}
                                        >
                                            {'prev 10 pages'}
                                        </button>
                                        :
                                        null
                                }


                                {
                                    hasPrevPage ?
                                        <button
                                            onClick={onPrevPage}>
                                            {'prev page'}
                                        </button>
                                        :
                                        null
                                }


                                {
                                    pagesNumbersToDisplay
                                        .map(page =>
                                            <button
                                                key={page}
                                                style={{ color: page == currentPage ? 'red' : 'black' }}
                                                onClick={() => setPageNumber(page)}
                                            >
                                                {page + 1}
                                            </button>)
                                }



                                {
                                    hasNextPage ?
                                        <button
                                            onClick={onNextPage}>
                                            {'next page'}
                                        </button>
                                        :
                                        null
                                }

                                {
                                    hasNextTenPages ?
                                        <button
                                            onClick={onNextTenPages}>
                                            {'next ten pages'}
                                        </button>
                                        :
                                        null
                                }
                            </>
                        )
                    }
                </LablebPagination>
            </LablebProvider>
        </div>
    ));



    await waitFor(() => screen.getByTestId('__test__container'))


    // await waitFor(() => expect(screen.queryByText('2')).toBeInTheDocument())
    // expect(screen.queryByText('1')).toBeInTheDocument();
    // expect(screen.queryByText('2')).toBeInTheDocument();
    // expect(screen.queryByText('3')).toBeInTheDocument();
    // expect(screen.queryByText('4')).toBeInTheDocument();
    // expect(screen.queryByText('5')).toBeInTheDocument();
});