import React, { useContext } from "react";
import { setPageNumber } from "../../actions/pagination/page-number.set";
import { GlobalStateContext } from "../../state/state.context";
import { LablebPaginationUIProps } from "./pagination.type";
import arrow_right from '../../assets/images/arrow_right.svg';
import double_arrow_right from '../../assets/images/double_arrow_right.svg';
import clsx from "clsx";
import { usePaginationStyle } from "./pagination.style";



export function LablebPaginationUI(props: LablebPaginationUIProps) {

    const {
        currentPage,
        hasNextPage,
        hasNextTenPages,
        hasPrevPage,
        hasPrevTenPages,
        onNextPage,
        onNextTenPages,
        onPrevPage,
        onPrevTenPages,
        pagesCount,
        pagesNumbersToDisplay,
        setPageNumber
    } = props;

    const classes = usePaginationStyle();
    const { state: { language } } = useContext(GlobalStateContext);


    return (
        <div
            dir={language == 'ar' ? 'rtl' : 'ltr'}
            className={classes.container}
        >


            {
                hasPrevTenPages ?
                    <button
                        className={classes.pageButton}
                        onClick={onPrevTenPages}
                    >
                        {
                            <img
                                className={clsx({
                                    [classes.reverseImage]: language == 'en'
                                })}
                                src={double_arrow_right}
                                alt="next-10"
                            />
                        }
                    </button>
                    :
                    null
            }


            {
                hasPrevPage ?
                    <button
                        className={classes.pageButton}
                        onClick={onPrevPage}>
                        <img
                            className={clsx({
                                [classes.reverseImage]: language == 'en'
                            })}
                            src={arrow_right}
                            alt="next"
                        />
                    </button>
                    :
                    null
            }


            {
                pagesNumbersToDisplay
                    .map(page =>
                        <button
                            key={page}
                            className={clsx(classes.pageButton, {
                                [classes.selectedPage]: page == currentPage
                            })}
                            onClick={() => setPageNumber(page)}
                        >
                            {page + 1}
                        </button>)
            }



            {
                hasNextPage ?
                    <button
                        className={classes.pageButton}
                        onClick={onNextPage}>
                        <img
                            className={clsx({
                                [classes.reverseImage]: language == 'ar'
                            })}
                            src={arrow_right}
                            alt="next"
                        />
                    </button>
                    :
                    null
            }

            {
                hasNextTenPages ?
                    <button
                        className={classes.pageButton}
                        onClick={onNextTenPages}>
                        {
                            <img
                                className={clsx({
                                    [classes.reverseImage]: language == 'ar'
                                })}
                                src={double_arrow_right}
                                alt="next-10"
                            />
                        }
                    </button>
                    :
                    null
            }

        </div>
    );
}