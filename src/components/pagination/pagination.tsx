import React from "react";
import { LablebPaginationErrorBoundary } from "./pagination.boundary";
import { LablebPaginationContainer } from "./pagination.container";

export function LablebPagination(props: React.ComponentProps<typeof LablebPaginationContainer>) {

    return (
        <LablebPaginationErrorBoundary>
            <LablebPaginationContainer {...props} />
        </LablebPaginationErrorBoundary>
    );
}