import React, { useCallback, useContext, useEffect } from "react";
import { setPageNumber } from "../../actions/pagination/page-number.set";
import { GlobalStateContext } from "../../state/state.context";
import { LablebPaginationProps, LablebPaginationUIProps } from "./pagination.type";
import { setPageSize } from "../../actions/pagination/page-size.set";
import { LablebPaginationUI } from "./pagination.ui";




export function LablebPaginationContainer(props: LablebPaginationProps) {

    const {
        children,
        pageSize,
        startPage,
    } = props;

    const { dispatch, state } = useContext(GlobalStateContext);


    useEffect(function updatePageSize() {

        if (pageSize && pageSize > 0)
            dispatch(setPageSize({ pageSize }));

    }, [pageSize]);


    useEffect(function updateCurrentPage() {

        if (startPage && startPage >= 0)
            dispatch(setPageNumber({ pageNumber: startPage }));

    }, [startPage]);


    const documentsCount = state.searchResults.found_documents;
    const _pageSize = state.pageSize;
    const _pageNumber = state.pageNumber;
    const pagesCount = Math.max(1, Math.ceil(documentsCount / _pageSize));


    let from = _pageNumber - 2;
    let to = _pageNumber + 3;

    if (to > pagesCount) {
        to = pagesCount;
        from = to - 5;
    }

    if (from < 0) {
        from = 0;
        to = from + 5;
    }

    const slicedPages = Array(pagesCount).fill(0).map((x, i) => i).slice(from, to);

    const hasNextPage = (_pageNumber + 1) < pagesCount;
    const hasNextTenPages = (_pageNumber + 11) < pagesCount;

    const hasPrevPage = _pageNumber > 0;
    const hasPrevTenPages = (_pageNumber - 10) >= 0;


    const nextPageClickHandler = useCallback(() => dispatch(setPageNumber({ pageNumber: _pageNumber + 1 })), [_pageNumber]);
    const nextTenClickHandler = useCallback(() => dispatch(setPageNumber({ pageNumber: _pageNumber + 10 })), [_pageNumber]);

    const prevPageClickHandler = useCallback(() => dispatch(setPageNumber({ pageNumber: _pageNumber - 1 })), [_pageNumber]);
    const prevTenClickHandler = useCallback(() => dispatch(setPageNumber({ pageNumber: _pageNumber - 10 })), [_pageNumber]);

    const pageChangeHandler = useCallback((pageNumber) => dispatch(setPageNumber({ pageNumber })), []);

    const UIProps: LablebPaginationUIProps = {
        currentPage: _pageNumber,
        hasNextPage,
        hasPrevPage,
        hasNextTenPages,
        hasPrevTenPages,
        onNextPage: nextPageClickHandler,
        onPrevPage: prevPageClickHandler,
        onNextTenPages: nextTenClickHandler,
        onPrevTenPages: prevTenClickHandler,
        pagesCount,
        pagesNumbersToDisplay: slicedPages,
        setPageNumber: pageChangeHandler
    }


    if (children) {
        return (
            <>
                {children(UIProps)}
            </>
        );
    }


    return (<LablebPaginationUI {...UIProps} />);
}