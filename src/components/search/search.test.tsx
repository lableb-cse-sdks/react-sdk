import React from 'react';
import { LablebProvider } from '../provider';
import { render, fireEvent, waitFor, screen, act } from '@testing-library/react'
import '@testing-library/jest-dom';
import { LablebSearch } from './search';
import { AutocompleteResult } from '../../ui-components/autocomplete-result/autocomplete-result';
import { LablebDocumentWithFeedback } from '@lableb/javascript-sdk/dist/types';


test('Render search component', async () => {

    render(
        <LablebProvider
            platformOptions={{
                platformName: 'PLATFORM_NAME',
                APIKey: 'API_KEY'
            }}
        >
            <LablebSearch />
        </LablebProvider>
    );
});



test('Render search component with options override 1', async () => {

    render(
        <LablebProvider
            platformOptions={{
                platformName: 'PLATFORM_NAME',
                APIKey: 'API_KEY'
            }}
        >
            <LablebSearch
                searchOptions={{
                    indexName: 'posts'
                }}
            />
        </LablebProvider>
    );
});


test('Render search component with options override 2', async () => {

    render(
        <LablebProvider
            platformOptions={{
                platformName: 'PLATFORM_NAME',
                APIKey: 'API_KEY'
            }}
        >
            <LablebSearch
                autocompleteOptions={{
                    indexName: 'posts'
                }}
            />
        </LablebProvider>
    );
});


test('Render search component with inline logo', async () => {

    render(
        <LablebProvider
            platformOptions={{
                platformName: 'PLATFORM_NAME',
                APIKey: 'API_KEY'
            }}
        >
            <LablebSearch
                inlineLogo
            />
        </LablebProvider>
    );
});

test('Render search component without filters', async () => {

    render(
        <LablebProvider
            platformOptions={{
                platformName: 'PLATFORM_NAME',
                APIKey: 'API_KEY'
            }}
        >
            <LablebSearch
                inlineFilters={false}
            />
        </LablebProvider>
    );
});

test('Render search component without auto search', async () => {

    render(
        <LablebProvider
            platformOptions={{
                platformName: 'PLATFORM_NAME',
                APIKey: 'API_KEY'
            }}
        >
            <LablebSearch
                preventAutoSearch
            />
        </LablebProvider>
    );
});



function CustomersSearchBar(props: any) {


    const { placeholder, value, setValue, onFocus, onBlur, onSubmit, onEnter } = props;

    return (
        <div>
            <input
                value={value}
                onFocus={onFocus}
                onBlur={onBlur}
                onClick={onFocus}
            />

            <button
                aria-label="search"
                onClick={onSubmit}
            >

                {'search'}
            </button>
        </div>
    );

}

test('Render search component with render props', async () => {

    render(
        <LablebProvider
            platformOptions={{
                platformName: 'PLATFORM_NAME',
                APIKey: 'API_KEY'
            }}
        >
            <LablebSearch
                searchOptions={{
                    limit: 9,
                }}
                autocompleteOptions={{
                    limit: 5,
                }}
                preventAutoSearch
            >
                {
                    ({
                        query,
                        setQuery,
                        autocompleteResults,
                        openAutocompleteResults,
                        setOpenAutocompleteResults,
                        sendAutocompleteFeedback,
                        searchAtLableb,
                        loading
                    }) => (
                        <div>

                            <CustomersSearchBar
                                onEnter={() => {
                                    searchAtLableb();
                                    setOpenAutocompleteResults(false)
                                }}
                                onSubmit={() => {
                                    searchAtLableb();
                                    setOpenAutocompleteResults(false)
                                }}
                                value={query}
                                setValue={setQuery}
                                onFocus={function _onFocus() {
                                    console.log('on focus')
                                    setOpenAutocompleteResults(true);
                                }}
                                onBlur={function _onBlur() {
                                    setTimeout(() => {
                                        setOpenAutocompleteResults(false);
                                    }, 200);
                                }}
                                placeholder={'DEMO_INPUT_PLACEHOLDER'}
                            />



                            {
                                (openAutocompleteResults && autocompleteResults.length) ?
                                    <div>
                                        {
                                            autocompleteResults
                                                ?.map((autocompleteResult, index) => (
                                                    <AutocompleteResult
                                                        key={`${autocompleteResult?.id}-${index}`}
                                                        onClick={function _autocompleteResultClickHandler(autocompleteResult: LablebDocumentWithFeedback) {

                                                            setQuery(autocompleteResult?.phrase);
                                                            setOpenAutocompleteResults(false);

                                                            if (autocompleteResult.id)
                                                                sendAutocompleteFeedback(autocompleteResult)
                                                        }}
                                                        autocompleteResult={autocompleteResult}
                                                        language={'en'}
                                                    />
                                                ))
                                        }
                                    </div>
                                    :
                                    null
                            }

                        </div>
                    )
                }
            </LablebSearch>
        </LablebProvider>
    );
});