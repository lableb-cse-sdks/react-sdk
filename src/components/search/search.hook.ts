import { useContext, useEffect, useState } from "react";
import { setSearchResults } from "../../actions/search/search-results.set";
import { GlobalStateContext } from "../../state/state.context";
import { debounce } from 'lodash';
import { SEARCH_DEBOUNCE } from "../../config/search.config";
import { OverrideSearchOptions } from "./search.type";
import { setPageNumber } from "../../actions/pagination/page-number.set";
import { incrementLoadingState } from "../../actions/loading/increment-loading";
import { decrementLoadingState } from "../../actions/loading/decrement-loading";
import { isLablebClientReady } from "../../utils";



export function useLablebSearch({
    query,
    overrideSearchOptions,
    preventAutoSearch,
}: {
    query: string,
    overrideSearchOptions?: OverrideSearchOptions
    preventAutoSearch?: boolean,
}) {

    const { state, dispatch } = useContext(GlobalStateContext);

    const [error, setError] = useState(null);
    if (error) throw error;

    const [debouncedSearchRef, setDebouncedSearchRef] = useState<any>(() => () => { });


    useEffect(function resetPageNumber() {
        dispatch(setPageNumber({ pageNumber: 0 }));
    }, [query]);



    function SearchAtLableb() {

        const { interceptors, ..._platformOptions } = state.platformOptions;

        isLablebClientReady(state)
            .then(() => {

                dispatch(incrementLoadingState({}));

                state.client
                    ?.search({
                        ..._platformOptions,
                        ...overrideSearchOptions,
                        facets: state.facets,
                        query: query || '*',
                        limit: Math.max(1, state.pageSize),
                        skip: Math.max(0, state.pageNumber * state.pageSize),
                    })
                    .then(({ response }) =>
                        dispatch(
                            setSearchResults({
                                searchResponse: {
                                    facets: response.facets || {},
                                    found_documents: response.found_documents || 0,
                                    results: response.results || []
                                },
                            })
                        )
                    )
                    .catch(console.error)
                    .finally(() => {
                        dispatch(decrementLoadingState({}))
                    });

            });

    }


    useEffect(function saveDebounceRef() {

        const debouncedFunc = debounce(SearchAtLableb, SEARCH_DEBOUNCE);

        // cancel any old debounced function
        if (debouncedSearchRef?.cancel && typeof debouncedSearchRef?.cancel == 'function')
            debouncedSearchRef?.cancel();


        // set the new debounced function
        setDebouncedSearchRef(() => debouncedFunc);

    }, [
        preventAutoSearch ? undefined : query,
        state.facets,
        state.platformOptions,
        state.client,
        overrideSearchOptions,
        state.pageNumber,
        state.pageSize,
    ]);


    useEffect(debouncedSearchRef, [debouncedSearchRef]);

    return {
        SearchAtLableb
    }
}