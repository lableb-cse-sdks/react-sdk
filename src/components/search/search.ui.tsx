import React, { useCallback, useContext, useEffect, useState } from "react";
import { GlobalStateContext } from "../../state/state.context";
import { LablebSearchProps, SearchRenderProps } from "./search.type";
import { useLablebSearch } from "./search.hook";
import { useLablebAutocomplete } from "./autocomplete.hook";
import { LablebDocumentWithFeedback } from "@lableb/javascript-sdk";
import { SearchBox } from "../../ui-components/search-box/search-box";
import { toggleFacet } from "../../actions/search/facets.toggle";
import { mergeResponseFacetsWithInputFacets } from "../search-filters/facets.utils";
import { LablebLogo } from "../../ui-components/lableb-logo/lableb-logo";
import { useLablebSearchStyles } from "./search.style";


export function LablebSearchUI(props: LablebSearchProps) {

  const {
    searchOptions,
    autocompleteOptions,
    inlineFilters,
    inlineLogo,
    preventAutoSearch,
    children,
    defaultQuery,
  } = props;

  const classes = useLablebSearchStyles();

  const { state, dispatch } = useContext(GlobalStateContext);

  const [openAutocompleteResults, setOpenAutocompleteResults] = useState(false);
  const [query, setQuery] = useState('');
  const [innerFilters, setInnerFilters] = useState('');

  const loading = Boolean(state.pendingRequests > 0);
  const autocompleteResults = state.autocompleteResults.results;


  /**
   * merge user selected facets
   */
  const mergedFacets = mergeResponseFacetsWithInputFacets({
    responseFacets: state.searchResults.facets,
    inputFacets: state.facets
  });


  const { SearchAtLableb } =
    useLablebSearch({ query, overrideSearchOptions: searchOptions, preventAutoSearch });
  useLablebAutocomplete({ query, overrideAutocompleteOptions: autocompleteOptions, innerFilters });


  useEffect(function defaultValueHandler(){
    if(defaultQuery){
      setQuery(defaultQuery);
      SearchAtLableb();
    }
  },[defaultQuery]);


  useEffect(function clearInnerFilter() {

    const documentPhrase = innerFilters?.split(':')[1];

    if (query != documentPhrase)
      setInnerFilters('');

  }, [query, innerFilters]);



  function autocompleteClickHandler(document: LablebDocumentWithFeedback) {

    sendAutocompleteFeedback(document);

    if (document.suggestion_type == 'filter') {
      if (document.type && document.phrase)
        setInnerFilters(`${document.type}:${document.phrase}`)
    }
  }


  const sendAutocompleteFeedback = useCallback((document: LablebDocumentWithFeedback) => {
    if (document.id)
      state.client?.feedback.autocomplete.single({ documentFeedback: document.feedback });
  }, [state.client]);


  const facetToggleHandler = useCallback(({ facetValue, facetName }) => {
    dispatch(toggleFacet({
      facetName,
      facetValue
    }))
  }, []);




  const childrenProps: SearchRenderProps = {
    query,
    setQuery,

    autocompleteResults,
    setOpenAutocompleteResults,
    openAutocompleteResults,
    sendAutocompleteFeedback,

    searchAtLableb: SearchAtLableb,

    loading,
  }


  if (children)
    return (
      <>
        {children(childrenProps)}
      </>
    ); childrenProps



  return (
    <div className={classes.searchBoxContainer}>

      <SearchBox
        openAutocompleteResults={openAutocompleteResults}
        setOpenAutocompleteResults={setOpenAutocompleteResults}
        autocompleteResults={autocompleteResults}
        onAutocompleteResultClick={autocompleteClickHandler}

        facets={mergedFacets}
        selectedFacets={state.facets ?? {}}
        onFacetToggle={facetToggleHandler}

        hasInlineLogo={Boolean(inlineLogo)}
        inlineFilters={Boolean(inlineFilters)}
        language={state.language}

        value={query}
        onChange={setQuery}

        loading={loading}
      />

      {
        !inlineLogo ?
          <LablebLogo poweredBy language={state.language} />
          :
        null
      }

    </div>
  );
}
