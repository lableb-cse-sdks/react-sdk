import { createUseStyles } from "react-jss";

export const useLablebSearchStyles=createUseStyles({
    "searchBoxContainer":{
        maxWidth: "340px"
    }
});