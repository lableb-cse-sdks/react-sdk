import React from "react";
import { SearchErrorBoundary } from "./search.boundary";
import { LablebSearchUI } from "./search.ui";


export function LablebSearch(props: React.ComponentProps<typeof LablebSearchUI>) {

    return (
        <SearchErrorBoundary>
            <LablebSearchUI {...props} />
        </SearchErrorBoundary>
    );
}