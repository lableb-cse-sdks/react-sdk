import { useContext, useEffect, useState } from "react";
import { debounce } from 'lodash';
import { AUTOCOMPLETE_DEBOUNCE } from "../../config/search.config";
import { GlobalStateContext } from "../../state/state.context";
import { setAutocompleteResults } from "../../actions/autocomplete/autocomplete-results.set";
import { OverrideAutocompleteOptions } from "./search.type";
import { incrementLoadingState } from "../../actions/loading/increment-loading";
import { decrementLoadingState } from "../../actions/loading/decrement-loading";
import { isLablebClientReady } from "../../utils";



export function useLablebAutocomplete({
    query,
    overrideAutocompleteOptions,
    innerFilters
}: {
    query: string,
    overrideAutocompleteOptions?: OverrideAutocompleteOptions,
    innerFilters: string,
}) {

    const { state, dispatch } = useContext(GlobalStateContext);


    const [debouncedAutocompleteRef, setDebouncedAutocompleteRef] = useState<any>(() => () => { });


    function AutocompleteAtLableb() {

        if (!query || query == '*') return;

        const { interceptors, ..._platformOptions } = state.platformOptions;

        isLablebClientReady(state)
            .then(() => {

                dispatch(incrementLoadingState({}));

                state.client
                    ?.autocomplete({
                        ..._platformOptions,
                        ...overrideAutocompleteOptions,
                        filter: innerFilters + (overrideAutocompleteOptions?.filter ?? ''),
                        query,
                    })
                    .then(({ response }) =>
                        dispatch(
                            setAutocompleteResults({
                                autocompleteResponse: response,
                            })
                        )
                    )
                    .catch(console.error)
                    .finally(() => dispatch(decrementLoadingState({})))

            });

    }


    useEffect(function debouncedAutocomplete() {

        const debouncedFunc = debounce(AutocompleteAtLableb, AUTOCOMPLETE_DEBOUNCE);

        // cancel any old debounced function
        if (debouncedAutocompleteRef?.cancel && typeof debouncedAutocompleteRef?.cancel == 'function')
            debouncedAutocompleteRef?.cancel();

        // set the new debounced function
        setDebouncedAutocompleteRef(() => debouncedFunc);

    }, [query, state.platformOptions, overrideAutocompleteOptions, innerFilters]);


    useEffect(debouncedAutocompleteRef, [debouncedAutocompleteRef]);

}