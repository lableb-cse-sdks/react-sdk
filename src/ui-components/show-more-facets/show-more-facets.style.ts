import { createUseStyles } from "react-jss";

export const useShowMoreFacetsStyles = createUseStyles({
  "expandMoreContainer": {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    marginTop: "15px"
  },
  "expandMoreButton": {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    border: "none",
    backgroundColor: "transparent",
    fontSize: "12px",
    color: "var(--main-text-color)",
    padding: "5px",
    borderRadius: "2px",
    transform: "all 0.4s ease-out",
    "&>img": { margin: "0 5px" },
    "&:hover": { cursor: "pointer", backgroundColor: "#eee" },
  },
  "reverseChevronBottom": { transform: "scaleY(-1)" }
});
