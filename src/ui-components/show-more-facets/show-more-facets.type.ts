import { Language } from "../../types/main";

export interface ShowMoreFacetsButtonProps {
    language: Language,
    onClick: () => void,
    isShrunk: boolean,
}
