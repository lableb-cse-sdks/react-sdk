import React from 'react';
import chevron_bottom from '../../assets/images/chevron_bottom.svg';
import clsx from 'clsx';
import { ShowMoreFacetsButtonProps } from './show-more-facets.type';
import { translations } from '../search-box/search.translation';
import { useShowMoreFacetsStyles } from './show-more-facets.style';


export function ShowMoreFacetsButton(props: ShowMoreFacetsButtonProps) {

    const {
        language,
        onClick,
        isShrunk
    } = props;

    const classes = useShowMoreFacetsStyles();

    return (
        <div className={classes.expandMoreContainer}>
            <button
                className={classes.expandMoreButton}
                onClick={onClick}
            >
                <span>
                    {
                        isShrunk ?
                            translations[language]['SHOW_MORE']
                            :
                            translations[language]['SHOW_LESS']
                    }
                </span>
                <img
                    className={clsx({
                        [classes.reverseChevronBottom]: !isShrunk
                    })}
                    src={chevron_bottom}
                    alt="more"
                />
            </button>
        </div>
    );
}
