import React from 'react';
import search from '../../assets/images/search.svg';
import clsx from 'clsx';
import { translations } from '../search-box/search.translation';
import { SearchIconProps } from './search-icon.type';
import { useSearchIconStyles } from './search-icon.style';


export function SearchIcon(props: SearchIconProps) {

    const {
        onClick,
        language,
    } = props;

    const classes = useSearchIconStyles();

    return (
        <img
            src={search}
            alt="search"
            className={clsx(classes.searchIcon, {
                [classes.searchIconRTL]: language == 'ar',
            })}
            onClick={onClick}
            title={translations[language]["SEARCH"]}
        />
    );
}