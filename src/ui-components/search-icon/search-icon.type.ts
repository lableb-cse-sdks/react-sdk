import { Language } from '../../types/main';

export interface SearchIconProps {
    onClick: () => void,
    language: Language,
}