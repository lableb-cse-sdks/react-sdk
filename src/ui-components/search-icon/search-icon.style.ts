import { createUseStyles } from "react-jss";

export const useSearchIconStyles = createUseStyles({
  "searchIcon": {
    padding: "5px",
    width: "20px",
    height: "20px",
    maxWidth: "20px",
    maxHeight: "20px",
    "&:hover": {
      backgroundColor: "#f0f0f0",
      borderRadius: "50%",
      cursor: "pointer"
    }
  },
  "searchIconRTL": { transform: "scaleX(-1)" },
});
