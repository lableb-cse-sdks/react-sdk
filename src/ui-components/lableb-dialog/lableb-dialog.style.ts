import { createUseStyles } from "react-jss";

export const useLablebDialogStyles = createUseStyles({
  "hiddenDisplay": { display: "none !important" },
  "dialogRoot": {
    color: "black",
    position: "fixed",
    zIndex: 100,
    top: "0",
    left: "0",
    width: "100%",
    height: "100%",
    background: "#00000080",
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
    borderRadius: "10px"
  },
  "dialog": {
    color: "black",
    minWidth: "380px",
    maxWidth: "80vw",
    background: "white",
    borderRadius: "10px",
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center",
    boxShadow:
      "0px 11px 15px -7px rgb(0 0 0 / 20%), 0px 24px 38px 3px rgb(0 0 0 / 14%), 0px 9px 46px 8px rgb(0 0 0 / 12%)"
  },
  "stretchContent": { alignItems: "stretch" },
  "dialogHeader": { color: "black", padding: "20px", position: 'relative' },
  "dialogBody": {
    color: "black",
    padding: "20px 0",
    borderTop: "1px solid #0000001f",
    borderBottom: "1px solid #0000001f",
    flexGrow: 1,
    width: "80vw",
    maxHeight: "70vh",
    overflow: "auto"
  },
  "closeIcon": {
    position: 'absolute',
    right: '20px',
    top: '16px',
    maxWidth: '35px',
    '&:hover': {
      cursor: 'pointer'
    }
  },
  "dialogActions": {
    color: "black",
    padding: "20px",
    display: "flex",
    justifyContent: "flex-end",
    alignItems: "center"
  },
  "dialogCloseButton": {
    color: "var(--main-text-color)",
    border: "none",
    backgroundColor: "transparent",
    "&:hover": { cursor: "pointer" },
  },
  "dialogAcceptButton": {
    color: "white",
    backgroundColor: "#26B6C9",
    border: "none",
    padding: "10px",
    borderRadius: "5px",
    margin: "0 5px",
    "&:hover": { cursor: "pointer" },
  },
  "fadeIn": {
    WebkitAnimation:
      "fade-in-bck 0.4s cubic-bezier(0.390, 0.575, 0.565, 1.000) both",
    animation: "fade-in-bck 0.4s cubic-bezier(0.390, 0.575, 0.565, 1.000) both"
  },
  "fadeOut": {
    WebkitAnimation:
      "fade-out-bck 0.7s cubic-bezier(0.250, 0.460, 0.450, 0.940) both",
    animation: "fade-out-bck 0.7s cubic-bezier(0.250, 0.460, 0.450, 0.940) both"
  },
});
