import React from "react";
import { useCallback, useEffect, useState } from "react";
import clsx from 'clsx';
import { LablebDialogProps } from "./lableb-dialog.type";
import { translations } from "../search-box/search.translation";
import { useLablebDialogStyles } from "./lableb-dialog.style";
import closeIcon from '../../assets/images/close.svg';



export function LablebDialog(props: LablebDialogProps) {

    const {
        body,
        header,
        open,
        onClose,
        stretchContent,
        showAcceptButton,
        onAccept,
        language
    } = props;

    const [openDialogStateTrigger, setOpenDialogStateTrigger] = useState(false);

    const classes = useLablebDialogStyles();

    useEffect(() => {

        if (open) {
            setOpenDialogStateTrigger(true)
        } else {
            setTimeout(() => {
                setOpenDialogStateTrigger(false)
            }, 500);
        }

    }, [open]);


    const stopPropagation = useCallback((event) => event.stopPropagation(), []);

    return (
        <div className={clsx(classes.dialogRoot, {
            [classes.hiddenDisplay]: !open,
        })}
            onClick={onClose}
        >

            <div
                onClick={stopPropagation}
                className={clsx(classes.dialog, {
                    [classes.hiddenDisplay]: !openDialogStateTrigger,
                    [classes.fadeIn]: openDialogStateTrigger,
                    [classes.fadeOut]: !openDialogStateTrigger,
                    [classes.stretchContent]: stretchContent,
                })}>

                {
                    props.hideContainers ?
                        body
                        :
                        (
                            <>
                                <div className={classes.dialogHeader}>
                                    {header}
                                    <img
                                        className={classes.closeIcon}
                                        src={closeIcon}
                                        onClick={onClose}
                                    />
                                </div>


                                <div className={classes.dialogBody}>
                                    {body}
                                </div>

                                {
                                    onAccept && showAcceptButton ?
                                        <div className={classes.dialogActions}>
                                            <button className={classes.dialogAcceptButton} onClick={onAccept}>
                                                {translations[language]['ACCEPT']}
                                            </button>
                                        </div>
                                        :
                                        null
                                }
                            </>
                        )
                }

            </div>

        </div>
    );
}