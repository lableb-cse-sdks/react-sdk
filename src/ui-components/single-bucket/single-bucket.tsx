import React from "react";
import clsx from "clsx";
import minus from '../../assets/images/minus.svg';
import plus from '../../assets/images/plus.svg';
import { SingleBucketProps } from './single-bucket.type';
import { useSingleBucketStyles } from './single-bucket.style';


export function SingleBucket(props: SingleBucketProps) {

    const {
        bucket,
        onBucketClick,
        selected,
        includeCount,
        language,
        useCheckbox,
    } = props;

    const classes = useSingleBucketStyles();

    if (useCheckbox) {
        return (
            <div className={classes.flexContainer}>
                <div className={classes.checkboxContainer}>

                    <input
                        className={classes.checkboxInput}
                        type="checkbox"
                        id={`${bucket.value}-${bucket.count}`}
                        checked={selected}
                        onChange={() => onBucketClick(bucket)}
                    />

                    <label
                        className={classes.checkboxLabel}
                        htmlFor={`${bucket.value}-${bucket.count}`}>

                    </label>

                    <label
                        className={classes.checkboxLabelWithText}
                        htmlFor={`${bucket.value}-${bucket.count}`}>
                        {bucket.value}
                    </label>

                </div>

                {
                    includeCount ?
                        <span className={classes.facetCount}>
                            {bucket?.count}
                        </span>
                        :
                        null
                }
            </div>
        );
    }


    return (
        <div
            className={clsx(classes.singleFacetValueContainer, {
                [classes.singleFacetValueContainerActive]: selected,
            })}
            onClick={() => onBucketClick(bucket)}
            dir={language == 'ar' ? 'rtl' : 'ltr'}
        >

            <span
                dir={language == 'ar' ? 'rtl' : 'ltr'}
                className={classes.facetValue}
            >
                <span>
                    {bucket.value}
                </span>

                <span className={classes.facetCount}>
                    {
                        includeCount ?
                            ` (${bucket?.count}) `
                            :
                            null
                    }
                </span>
            </span>

            {
                selected ?
                    <img
                        className={classes.removeIcon}
                        src={minus}
                        alt="remove"
                    />
                    :
                    <img
                        className={classes.plusIcon}
                        src={plus}
                        alt="add"
                    />
            }

        </div>
    );
}