import { createUseStyles } from "react-jss";

export const useSingleBucketStyles = createUseStyles({
  "singleFacetValueContainer": {
    padding: "1px 3px",
    border: "1.5px solid var(--main-color)",
    borderRadius: "2px",
    margin: "2px",
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
    "&:hover": { cursor: "pointer" },
  },
  "singleFacetValueContainerActive": {
    backgroundColor: "var(--main-text-color)",
    "& *": { color: "white !important" },
  },
  "facetValue": {
    lineHeight: 1,
    whiteSpace: "nowrap",
    fontSize: "1rem",
    color: "var(--main-text-color)",
    textTransform: "lowercase",
    display: "flex",
    justifyContent: "center",
    alignItems: "center"
  },
  "facetCount": {
    padding: "0 2px",
    lineHeight: 1,
    whiteSpace: "nowrap",
    fontSize: "0.8rem",
    color: "var(--main-text-color)"
  },
  "plusIcon": {
    marginInlineStart: "5px",
    width: "1.2rem",
    "&:hover": { cursor: "pointer" },
  },
  "removeIcon": {
    marginInlineStart: "5px",
    width: "1.2rem",
    "&:hover": { cursor: "pointer" },
  },
  "checkboxContainer": {
    display: "flex",
    justifyContent: "flex-start",
    alignItems: "center",
    padding: "1px 0"
  },
  "checkboxInput": {
    display: "none !important",
    "&:checked+label": { backgroundColor: "var(--main-text-color)" },
    "&:checked+label:after": {
      content: '""',
      display: "block",
      position: "absolute",
      top: "-2px",
      left: "-2px",
      width: "12px",
      height: "6px",
      border: "2px solid white",
      borderRight: "none",
      borderTop: "none",
      transform: "translate(3px, 4px) rotate(-45deg)"
    },
  },
  "checkboxLabel": {
    display: "inline-block",
    color: "var(--main-text-color)",
    fontSize: "10px",
    cursor: "pointer",
    width: "18px",
    height: "18px",
    minWidth: "18px",
    minHeight: "18px",
    border: "1.5px solid var(--main-text-color)",
    borderRadius: "2px",
    position: "relative",
    marginInlineEnd: "5px"
  },
  "checkboxLabelWithText": {
    color: "var(--main-text-color)",
    fontSize: "1rem",
    cursor: "pointer"
  },
  "flexContainer": {
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
    width: "100%"
  }
});
