import { BucketObject, Language } from "../../types/main";


export interface SingleBucketProps {
    bucket: BucketObject,
    onBucketClick: (bucket: BucketObject) => void,
    selected: boolean,
    includeCount?: boolean,
    language: Language,
    useCheckbox?: boolean,
}
