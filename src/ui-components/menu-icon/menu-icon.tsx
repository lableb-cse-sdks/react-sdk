import React from 'react';
import { translations } from '../search-box/search.translation';
import menu from '../../assets/images/menu.svg';
import { MenuIconProps } from './menu-icon.type';
import { useMenuIconStyles } from './menu-icon.style';


export function MenuIcon(props: MenuIconProps) {

    const {
        language,
        onClick,
        selectedFacetsCount,
    } = props;

    const classes = useMenuIconStyles();

    return (
        <div className={classes.menuIconContainer}>
            <img
                src={menu}
                alt="menu"
                title={translations[language]["SEARCH_FILTERS"]}
                className={classes.menuIcon}
                onClick={onClick}
            />
            {
                selectedFacetsCount > 0 ?
                    <span className={classes.count}>
                        {selectedFacetsCount}
                    </span>
                    :
                    null
            }

        </div>
    );
}