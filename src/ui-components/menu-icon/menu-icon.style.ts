import { createUseStyles } from "react-jss";

export const useMenuIconStyles = createUseStyles({
    "menuIcon": {
      width: "18px",
      height: "18px",
      maxWidth: "18px",
      maxHeight: "18px",
      padding: "5px",
      "&:hover": {
        cursor: "pointer",
        borderRadius: "50%",
        backgroundColor: "#eee"
      },
    },
 
    "menuIconContainer": {
      position: "relative",
      display: "flex",
      justifyContent: "center",
      alignItems: "center"
    },
    "count": {
      position: "absolute",
      top: "-1px",
      right: "-2px",
      fontSize: "8px",
      borderRadius: "50%",
      width: "14px",
      height: "14px",
      backgroundColor: "#345b73",
      color: "white",
      display: "flex",
      justifyContent: "center",
      alignItems: "center",
      lineHeight: 1,
      boxShadow: "0px 0px 10px #d6d6d6",
      animationName: "fadeIn",
      animationTimingFunction: "ease-out",
      animationDuration: "1s",
      animationFillMode: "forwards"
    },
  })
  