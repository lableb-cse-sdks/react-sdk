import { SelectedFacets } from '@lableb/javascript-sdk';
import { LablebSearchFacets, Language } from '../../types/main';


export interface SearchFacetsProps {
    facets: LablebSearchFacets,
    language: Language,
    facetsCount?: number,
    toggleFacetsHandler: (params: { facetName: string, facetValue: string }) => void,
    includeBucketsSize?: boolean,
    useButtonForMoreBuckets?: boolean,
    singleColumn?: boolean,
    includeCount?: boolean,
    bucketsSize?: number,
    useCheckbox?: boolean,
    selectedFacets: SelectedFacets,
}