import React, { useCallback, useState } from "react";
import { inverse } from '../../utils';
import { SearchFacetsProps } from './search-facets.type';
import { SingleFacetWithBuckets } from '../single-facet-with-buckets/single-facet-with-buckets';
import { ShowMoreFacetsButton } from '../show-more-facets/show-more-facets';


export function SearchFacets(props: SearchFacetsProps) {

    const {
        facets,
        language,
        facetsCount,
        toggleFacetsHandler,
        includeBucketsSize,
        useButtonForMoreBuckets,
        singleColumn,
        includeCount,
        bucketsSize,
        useCheckbox,
        selectedFacets,
    } = props;

    const SHRINK_FACETS_SIZE = facetsCount || 3;

    const [shrinkFacets, setShrinkFacets] = useState(true);

    const showMoreFacetsHandler = useCallback(function _showFacetsToggleHandler() {
        setShrinkFacets(inverse);
    }, []);


    const facetsKeys = Object.keys(facets).filter(facetName => facets?.[facetName]?.buckets?.length);



    return (
        <>
            <div
                dir={language == 'ar' ? 'rtl' : 'ltr'}
            >

                {
                    facetsKeys
                        .slice(0, shrinkFacets ? SHRINK_FACETS_SIZE : facetsKeys.length)
                        .map((facetName, index) => (
                            <SingleFacetWithBuckets
                                key={`${facetName}-${index}`}
                                buckets={facets?.[facetName]?.buckets ?? []}
                                facetName={facetName}
                                onBucketClick={function _bucketClickHandler(bucket) {
                                    toggleFacetsHandler({ facetName, facetValue: bucket.value });
                                }}
                                language={language}
                                includeBucketsSize={includeBucketsSize}
                                useButtonForMoreBuckets={useButtonForMoreBuckets}
                                singleColumn={singleColumn}
                                includeCount={includeCount}
                                bucketsSize={bucketsSize}
                                useCheckbox={useCheckbox}
                                selectedBuckets={selectedFacets?.[facetName] ?? []}
                            />
                        ))
                }


                {
                    facetsKeys.length > SHRINK_FACETS_SIZE ?
                        <ShowMoreFacetsButton
                            language={language}
                            isShrunk={shrinkFacets}
                            onClick={showMoreFacetsHandler}
                        />
                        :
                        null
                }


            </div>
        </>
    );
}