import React from 'react';
import clsx from 'clsx';
import { useCallback, useState } from 'react';
import { translations } from './search.translation';
import { SearchIcon } from '../search-icon/search-icon';
import { MenuIcon } from '../menu-icon/menu-icon';
import { LablebLogo } from '../lableb-logo/lableb-logo';
import { AutocompleteResult } from '../autocomplete-result/autocomplete-result';
import { SearchFacets } from '../search-facets/search-facets';
import { SearchBoxProps } from './search-box.type';
import { inverse, sum } from '../../utils';
import { LablebDocumentWithFeedback } from '@lableb/javascript-sdk';
import { useSearchBoxStyles } from './search-box.style';


export function SearchBox(props: SearchBoxProps) {

    const {
        facets,
        language,
        inlineFilters,
        hasInlineLogo,
        autocompleteResults,
        value,
        onChange,
        selectedFacets,
        onFacetToggle,
        onAutocompleteResultClick,
        loading,
        openAutocompleteResults,
        setOpenAutocompleteResults,
    } = props;

    const classes = useSearchBoxStyles();

    const [openFilters, setOpenFilters] = useState(false);



    const submitSearchHandler = useCallback(function _submitSearchHandler() {
        setOpenFilters(false);
        setOpenAutocompleteResults(false);
        onChange(value);
    }, [value]);


    const filtersClickHandler = useCallback(function _filtersClickHandler() {
        setOpenFilters(inverse);
        setOpenAutocompleteResults(false);
    }, []);


    const inputFocusHandler = useCallback(function onFocus() {
        setOpenAutocompleteResults(true);
        setOpenFilters(false);
    }, []);

    const inputBlurHandler = useCallback(function onBlur() {
        setTimeout(() => {
            setOpenAutocompleteResults(false);
        }, 200);
    }, []);


    const inputChangeHandler = useCallback(function _inputChangeHandler(event) {
        onChange(event.target.value);
    }, []);

    const autocompleteResultClickHandler = useCallback(function _autocompleteResultClickHandler(autocompleteResult: LablebDocumentWithFeedback) {
        onChange(autocompleteResult?.phrase);
        setOpenAutocompleteResults(false);
        onAutocompleteResultClick(autocompleteResult);
    }, [onAutocompleteResultClick]);


    const hasFilledFacets = Boolean(
        Object.keys(facets)
            ?.filter(facetName => facets[facetName]?.buckets?.length)
            ?.length
    );

    const hasSelectedFacets = Boolean(
        Object.keys(selectedFacets)
            .filter(facetName => selectedFacets[facetName].length)
            .length
    );

    const selectedFacetsCount = Object.keys(selectedFacets)
        .map(facetName => selectedFacets[facetName].length)
        .reduce(sum, 0);


    return (
        <div className={classes.root}>

            <div
                dir={language == 'ar' ? 'rtl' : 'ltr'}
                className={clsx(classes.container, {
                    [classes.containerWithAutocompleteResults]: (value && openAutocompleteResults && autocompleteResults.length) || openFilters
                })}
            >

                <SearchIcon
                    onClick={submitSearchHandler}
                    language={language}
                />


                <input
                    onFocus={inputFocusHandler}
                    onBlur={inputBlurHandler}
                    value={value}
                    onChange={inputChangeHandler}
                    className={classes.input}
                    spellCheck={false}
                    placeholder={translations[language]['INPUT_PLACEHOLDER']}
                />


                {
                    inlineFilters ?
                        <MenuIcon
                            language={language}
                            onClick={filtersClickHandler}
                            selectedFacetsCount={selectedFacetsCount}
                        />
                        : null
                }


                {
                    hasInlineLogo && inlineFilters ?
                        <hr className={classes.hr} />
                        : null
                }

                {
                    hasInlineLogo ?
                        <LablebLogo language={language} />
                        : null
                }


            </div>

            {
                value && openAutocompleteResults && autocompleteResults?.length ?
                    <div className={classes.autocompleteResultsContainer}>
                        {
                            autocompleteResults
                                .map((autocompleteResult, index) => (
                                    <AutocompleteResult
                                        key={`${autocompleteResult?.id}-${index}`}
                                        onClick={autocompleteResultClickHandler}
                                        autocompleteResult={autocompleteResult}
                                        language={language}
                                    />
                                ))
                        }

                    </div>
                    : null
            }


            {
                inlineFilters &&
                    openFilters &&
                    !openAutocompleteResults &&
                    (hasFilledFacets || hasSelectedFacets)
                    ?
                    <div className={classes.searchFiltersContainer}>
                        <SearchFacets
                            facets={facets}
                            language={language}
                            toggleFacetsHandler={onFacetToggle}
                            selectedFacets={selectedFacets}
                        />
                    </div>
                    :
                    null
            }


            {
                <div
                    className={clsx(classes.loadingLine, {
                        [classes.loadingLineFadeOut]: !loading
                    })}>
                </div>
            }

        </div>
    );
}


