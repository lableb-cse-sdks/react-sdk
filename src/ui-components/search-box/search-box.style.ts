import { createUseStyles } from "react-jss";

export const useSearchBoxStyles = createUseStyles({
    "root": { position: "relative" },
    "container": {
        display: "flex",
        alignItems: "center",
        border: "1.5px solid var(--main-color)",
        borderRadius: "4px",
        width: "340px",
        backgroundColor: "white",
        padding: "5px 5px",
        maxWidth: "340px",
        boxSizing: "border-box",
        "&>*": { margin: "0 3px" },
    },
    "containerWithAutocompleteResults": {
        borderBottom: "0",
        borderBottomLeftRadius: "0",
        borderBottomRightRadius: "0"
    },
    "hr": {
        height: "15px",
        backgroundColor: "var(--main-color)",
        width: "1px",
        border: "none"
    },
    "input": {
        minWidth: "200px",
        padding: "5px 0",
        border: "none",
        color: "var(--main-text-color)",
        fontSize: "16px",
        lineHeight: 1,
        flexGrow: 1,
       "&:focus": { outline: "none !important", boxShadow: "none !important" },
        "&:focus-visible": { outline: "none !important", boxShadow: "none !important"  },
        "&:focus-within": { outline: "none !important", boxShadow: "none !important"  },
        "&:focus-visited": { outline: "none !important", boxShadow: "none !important"  },
    },
    "autocompleteResultsContainer": {
        border: "1.5px solid var(--main-color)",
        width: "340px",
        maxWidth: "340px",
        boxSizing: "border-box",
        borderRadius: "4px",
        borderTop: "none",
        borderTopLeftRadius: "0",
        borderTopRightRadius: "0",
        position: "absolute",
        backgroundColor: "white",
        zIndex: 1000000
    },
    "searchFiltersContainer": {
        overflow: "hidden",
        border: "1.5px solid var(--main-color)",
        width: "340px",
        maxWidth: "340px",
        boxSizing: "border-box",
        borderRadius: "4px",
        borderTop: "none",
        borderTopLeftRadius: "0",
        borderTopRightRadius: "0",
        position: "absolute",
        padding: "10px",
        backgroundColor: "white",
        zIndex: 1000000
    },
    "loadingLine": {
        position: "relative",
        bottom: "3px",
        left: "0.2px",
        width: "calc(100% - 1px)",
        backgroundColor: "#d8d8d8",
        height: "2px",
        borderRadius: "4px",
        "&::after": {
            content: '""',
            position: "absolute",
            top: "0",
            left: "0",
            height: "2px",
            backgroundColor: "var(--main-text-color)",
            width: "0",
            animation: "getWidth 2s ease-in infinite",
            borderRadius: "4px"
        },
    },

    "loadingLineFadeOut::after": { display: "none" },
    "loadingLineFadeOut": {
        animation: "fadeout 0.2s ease-in forwards !important"
    },

});
