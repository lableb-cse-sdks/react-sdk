import { createUseStyles } from "react-jss";

export const useSingleFacetWithBucketsStyles = createUseStyles({
  "facetItem": { display: "flex", flexDirection: "column", margin: "5px 0" },
  "facetName": {
    margin: "0",
    color: "var(--main-text-color)",
    fontSize: "1rem",
    marginBottom: "2px",
    textTransform: "capitalize",
    fontWeight: 600,
    alignSelf: "flex-start"
  },
  "facetNameContainer": {
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center"
  },
  "bucketSize": { color: "var(--main-color)", fontSize: "0.9rem" }
});
