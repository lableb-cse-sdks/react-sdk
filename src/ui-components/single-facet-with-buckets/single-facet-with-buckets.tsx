import React from 'react';
import { Buckets } from '../buckets/buckets';
import { translations } from '../search-box/search.translation';
import { useSingleFacetWithBucketsStyles } from './single-facet-with-buckets.style';
import { SingleFacetWithBucketsProps } from './single-facet-with-buckets.type';


export function SingleFacetWithBuckets(props: SingleFacetWithBucketsProps) {

    const {
        facetName,
        onBucketClick,
        language,
        buckets,
        includeBucketsSize,
        includeCount,
        singleColumn,
        useButtonForMoreBuckets,
        bucketsSize,
        useCheckbox,
        selectedBuckets,
    } = props;

    const classes = useSingleFacetWithBucketsStyles();

    return (
        <div className={classes.facetItem}>

            <div className={classes.facetNameContainer}>

                <p className={classes.facetName}>
                    {facetName}
                </p>

                {
                    includeBucketsSize ?
                        <span className={classes.bucketSize}>
                            {`${buckets.length} ${translations[language]['OPTIONS']}`}
                        </span>
                        :
                        null
                }

            </div>


            <Buckets
                language={language}
                buckets={buckets}
                onBucketClick={onBucketClick}
                useButtonForMoreBuckets={useButtonForMoreBuckets}
                singleColumn={singleColumn}
                includeCount={includeCount}
                shrinkSize={bucketsSize}
                useCheckbox={useCheckbox}
                selectedBuckets={selectedBuckets}
            />

        </div>
    );
}