import React from 'react';
import clsx from 'clsx';
import { useState } from 'react';
import chevron from '../../assets/images/chevron.svg';
import chevron_bottom from '../../assets/images/chevron_bottom.svg';
import { translations } from '../search-box/search.translation';
import { BucketObject } from '../../types/main';
import { BucketsProps } from './buckets.type';
import { SingleBucket } from '../single-bucket/single-bucket';
import { useBucketsStyles } from './buckets.style';


export function Buckets(props: BucketsProps) {

    const {
        buckets,
        language,
        useButtonForMoreBuckets,
        singleColumn,
        onBucketClick,
        shrinkSize,
        includeCount,
        useCheckbox,
        selectedBuckets,
    } = props;

    const classes = useBucketsStyles();

    const [shrink, setShrink] = useState(true);

    const firstBucketLength = buckets?.[0]?.value?.length ?? 0;
    const secondBucketLength = buckets?.[1]?.value?.length ?? 0;
    const thirdBucketLength = buckets?.[2]?.value?.length ?? 0;

    const guessedShrinkSize =
        (firstBucketLength + secondBucketLength + thirdBucketLength < 28) ?
            3
            :
            (firstBucketLength + secondBucketLength < 28) ? 2 : 1;


    const SHRINK_SIZE = shrinkSize || guessedShrinkSize;


    return (
        <>
            <div
                className={clsx(classes.facetValuesContainer, {
                    [classes.facetValuesContainerColumn]: singleColumn,
                })}
            >
                {
                    buckets
                        ?.slice(0, shrink ? SHRINK_SIZE : buckets.length)
                        ?.map((bucket: BucketObject, index: number) => (
                            <SingleBucket
                                key={`${bucket?.value}-${index}-${bucket?.count}`}
                                bucket={bucket}
                                onBucketClick={onBucketClick}
                                selected={Boolean(selectedBuckets.includes(bucket.value))}
                                language={language}
                                includeCount={includeCount}
                                useCheckbox={useCheckbox}
                            />
                        ))
                }


                {
                    buckets.length > SHRINK_SIZE ?
                        !shrink ?
                            (
                                useButtonForMoreBuckets ?
                                    <div className={clsx(classes.expandFacetsValuesBtnContainer, {
                                        [classes.expandFacetsValuesBtnContainerStartAlign]: singleColumn
                                    })}>
                                        <button onClick={() => setShrink(true)}>
                                            {translations[language]['SHOW_LESS']}
                                            <img className={classes.flipChevron} src={chevron_bottom} alt="show less" />
                                        </button>
                                    </div>
                                    :
                                    <img
                                        className={clsx(classes.moreFacetsIcon, {
                                            [classes.lessFacetsIcon]: language == 'en'
                                        })}
                                        src={chevron}
                                        alt="less"
                                        onClick={() => setShrink(true)}
                                    />
                            )
                            :
                            (
                                useButtonForMoreBuckets ?
                                    <div className={clsx(classes.expandFacetsValuesBtnContainer, {
                                        [classes.expandFacetsValuesBtnContainerStartAlign]: singleColumn
                                    })}>
                                        <button onClick={() => setShrink(false)}>
                                            {translations[language]['SHOW_MORE']}
                                            <img src={chevron_bottom} alt="show more" />
                                        </button>
                                    </div>
                                    :
                                    <img
                                        className={clsx(classes.moreFacetsIcon, {
                                            [classes.lessFacetsIcon]: language == 'ar'
                                        })}
                                        src={chevron}
                                        alt="more"
                                        onClick={() => setShrink(false)}
                                    />
                            )
                        :
                        null
                }

            </div>
        </>
    );
}
