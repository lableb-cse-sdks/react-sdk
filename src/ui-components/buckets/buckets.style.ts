import { createUseStyles } from "react-jss";

export const useBucketsStyles = createUseStyles({
  "facetValuesContainer": {
    display: "flex",
    justifyContent: "flex-start",
    alignItems: "center",
    flexWrap: "wrap",
    margin: "4px 0",
    "&>*:first-child": { marginLeft: "0" },
  },
  "facetValuesContainerColumn": {
    flexDirection: "column",
    alignItems: "flex-start",
    "&>*": { marginLeft: "0 !important" },
  },
  "moreFacetsIcon": {
    width: "10px",
    maxWidth: "10px",
    marginInlineStart: "5px",
    "&:hover": { cursor: "pointer" },
  },
  "lessFacetsIcon": { transform: "scaleX(-1)" },
  "expandFacetsValuesBtnContainer": {
    width: "100%",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    padding: "5px 0 5px 0",
    "&>button": {
      border: "none",
      backgroundColor: "transparent",
      color: "var(--main-text-color)",
      fontSize: "0.8rem",
      cursor: "pointer",
      display: "flex",
      justifyContent: "center",
      alignItems: "center",
      "&>img": { padding: "0 5px" },
    },
  },
  "flipChevron": { transform: "scaleY(-1)" },
  "expandFacetsValuesBtnContainerStartAlign": { justifyContent: "flex-start" }
});
