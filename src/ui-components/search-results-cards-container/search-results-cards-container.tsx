import clsx from "clsx";
import React, { ReactNode } from "react";
import { useSearchResultsCardsContainer } from "./search-results-cards-container.style";

export function SearchResultsCardsContainer({
    children,
    shrink,
}: {
    shrink?: boolean,
    children: ReactNode
}) {

    const classes = useSearchResultsCardsContainer();

    return (
        <div className={clsx(classes.searchResultsContainer, {
            [classes.shrinkContainer]: shrink,
        })}>
            {children}
        </div>
    );
}