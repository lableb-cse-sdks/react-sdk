import { createUseStyles } from "react-jss";

export const useSearchResultsCardsContainer = createUseStyles({
  "searchResultsContainer": {
    display: "grid",
    gridTemplateColumns: "repeat(auto-fit, 300px)",
    gridTemplateRows: "320px",
    gridAutoRows: "320px",
    gridGap: "20px",
    justifyContent: "center",
    justifyItems: "center"
  },
  "shrinkContainer": {
    gridTemplateColumns: "repeat(auto-fit, 300px)",
    gridTemplateRows: "200px",
    gridAutoRows: "200px",
  },
});
