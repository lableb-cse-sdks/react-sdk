import { createUseStyles } from 'react-jss'

export const useSearchResultCardStyles = createUseStyles({
    "container": {
        width: "280px",
        height: "320px",
        display: "grid",
        gridTemplateColumns: "280px",
        gridTemplateRows: "150px auto auto auto",
        cursor: "pointer",
        boxShadow: "0px 0px 10px #d8d8d8",
        borderRadius: "2px",
        backgroundColor: "white"
    },
    "shrinkContainer": {
        height: "200px",
    },
    "image": {
        maxWidth: "100%",
        borderTopLeftRadius: "4px",
        borderTopRightRadius: "4px"
    },
    "placeholderImage": { justifySelf: "center", maxHeight: "150px" },
    "loadingImage": { margin: "auto" },
    "title": {
        whiteSpace: "nowrap",
        maxWidth: "250px",
        textOverflow: "ellipsis",
        overflow: "hidden",
        color: "var(--main-text-color)",
        fontSize: "1rem",
        padding: "0 15px",
        justifySelf: "start",
        alignSelf: "center",
        margin: "0"
    },
    "description": {
        fontSize: "0.8rem",
        padding: "0 15px",
        color: "var(--main-color)",
        maxHeight: "38px",
        overflow: "hidden",
        alignSelf: "center"
    },
    "relatedContainer": {
        display: "flex",
        justifyContent: "space-between",
        alignItems: "center",
        padding: "4px 15px",
        margin: "0"
    },
    "price": { color: "var(--main-text-color)", fontSize: "0.8rem" },
    "relatedButton": {
        backgroundColor: "transparent",
        border: "none",
        color: "var(--main-text-color)",
        fontSize: "0.8rem",
        cursor: "pointer"
    }
});