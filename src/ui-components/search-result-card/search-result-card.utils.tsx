import React, { ReactNode } from "react";

export function loadImageElement({
    imageURL,
    timeoutT,
    imageProps,
    placeholderImage,
}: {
    imageURL: string,
    timeoutT?: number,
    imageProps: any,
    placeholderImage: ReactNode
}) {
    return new Promise(function (resolve, reject) {

        let timeout = timeoutT || 60000;
        let timer: any;

        let originalImage = new Image();

        originalImage.onerror = originalImage.onabort = function () {
            clearTimeout(timer);
            resolve(placeholderImage);
        };

        originalImage.onload = function () {
            clearTimeout(timer);
            resolve(
                <img
                    src={imageURL}
                    {...imageProps}
                />
            );
        };

        timer = setTimeout(function () {
            // reset .src to invalid URL so it stops previous
            // loading, but doesn't trigger new load
            originalImage.src = "//!!!!/test.jpg";
            resolve(placeholderImage);
        }, timeout);

        originalImage.src = imageURL;
    });
}