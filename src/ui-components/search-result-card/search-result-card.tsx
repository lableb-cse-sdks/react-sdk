import React, { useEffect, useState } from "react";
import { LablebDocumentWithFeedback, LablebDocumentWithRecommendFeedback } from "@lableb/javascript-sdk";
import { useCallback } from "react";
import { Language } from "../../types/main";
import placeholderImage from '../../assets/images/placeholder.png';
import loadingImage from '../../assets/images/loading.gif';
import { loadImageElement } from "./search-result-card.utils";
import { translations } from "../search-box/search.translation";
import { useSearchResultCardStyles } from "./search-result-card.style";
import clsx from 'clsx';


function PlaceholderImageElement() {

    const classes = useSearchResultCardStyles();

    return (
        <img
            className={clsx(classes.image, classes.placeholderImage)}
            src={placeholderImage}
            alt={'placeholder'}
        />
    );
}

function LoadingImageElement() {

    const classes = useSearchResultCardStyles();

    return (
        <img
            className={clsx(classes.image, classes.loadingImage)}
            src={loadingImage}
            alt={'loading'}
        />
    );
}


export function SearchResultCard({
    document,
    onClick,
    language,
    hideRelatedButton,
    onRelatedClick,
    shrink,
}: {
    document: LablebDocumentWithFeedback | LablebDocumentWithRecommendFeedback,
    onClick: (document: LablebDocumentWithFeedback | LablebDocumentWithRecommendFeedback) => void,
    onRelatedClick?: (document: LablebDocumentWithFeedback | LablebDocumentWithRecommendFeedback) => void,
    language: Language,
    hideRelatedButton?: boolean,
    shrink?: boolean,
}) {

    const classes = useSearchResultCardStyles();


    const clickHandler = useCallback(() => onClick(document), [document]);
    const relatedClickHandler = useCallback((event) => {
        event.stopPropagation();
        onRelatedClick && onRelatedClick(document);
    }, [document]);

    const [searchResultImage, setSearchResultImage] = useState<any>(<LoadingImageElement />);


    useEffect(function loadResultImage() {

        loadImageElement({
            imageURL: document?.image,
            placeholderImage: <PlaceholderImageElement />,
            imageProps: {
                className: classes.image,
                alt: document?.title,
            },
        })
            .then(image => setSearchResultImage(image))
            .catch(console.error);

    }, []);



    return (
        <div
            onClick={clickHandler}
            className={clsx(classes.container, {
                [classes.shrinkContainer]: shrink
            })}
            dir={language == 'ar' ? 'rtl' : 'ltr'}
        >

            {searchResultImage}

            <p
                title={document?.title}
                className={classes.title}
            >
                {document?.title}
            </p>

            <p className={classes.description}>
                {document?.description?.split(' ')?.slice(0, 10)?.join(' ')}
            </p>

            <div className={classes.relatedContainer}>
                <span className={classes.price}>
                    {document?.price}
                </span>

                {
                    hideRelatedButton ?
                        null
                        :
                        <button
                            className={classes.relatedButton}
                            onClick={relatedClickHandler}>
                            {translations[language]['RELATED']}
                        </button>
                }

            </div>

        </div>
    );
}