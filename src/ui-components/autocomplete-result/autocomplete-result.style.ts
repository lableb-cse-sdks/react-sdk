import { createUseStyles } from "react-jss";

export const useAutocompleteResultStyles = createUseStyles({
  "autocompleteSingleResult": {
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
    padding: "5px 10px",
    textDecoration: "none",
    "&:hover": { cursor: "pointer" },
    "&>p": {
      margin: "0",
      fontSize: "0.9rem",
      color: "var(--main-text-color)",
      whiteSpace: "nowrap",
      maxWidth: "92%",
      textOverflow: "ellipsis",
      overflow: "hidden"
    },
    "&>span": {
      fontSize: "0.9rem",
      color: "var(--main-text-color)",
      '& img': {
        width: "10px", maxWidth: "10px"
      }
    },
  },
});
