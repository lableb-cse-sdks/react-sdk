import { LablebDocumentWithFeedback } from '@lableb/javascript-sdk';
import { Language } from '../../types/main';


export interface AutocompleteResultProps {
    autocompleteResult: any,
    language: Language,
    onClick: (autocompleteResult: LablebDocumentWithFeedback) => void,
}