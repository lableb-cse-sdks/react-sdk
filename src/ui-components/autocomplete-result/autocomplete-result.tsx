import React from 'react';
import { AutocompleteResultProps } from './autocomplete-result.type';
import navigationalIcon from '../../assets/images/navigational.svg';
import filterIcon from '../../assets/images/filter.svg';
import { useAutocompleteResultStyles } from './autocomplete-result.style';

export function AutocompleteResult(props: AutocompleteResultProps) {

    const {
        autocompleteResult,
        language,
        onClick,
    } = props;

    const classes = useAutocompleteResultStyles();

    return (
        <a
            href={autocompleteResult?.suggestion_type == 'navigational' ? autocompleteResult?.url : null}
            target="_blank"
            className={classes.autocompleteSingleResult}
            onClick={() => onClick(autocompleteResult)}
            dir={language == 'ar' ? 'rtl' : 'ltr'}
        >
            <p>{autocompleteResult?.phrase}</p>
            <span>
                {
                    autocompleteResult?.suggestion_type == 'navigational' ?
                        <img src={navigationalIcon} alt="navigational" />
                        :
                        autocompleteResult?.suggestion_type == 'filter' ?
                            <img src={filterIcon} alt="filter" />
                            :
                            autocompleteResult?.suggestion_type
                }
            </span>
        </a>
    );
}
