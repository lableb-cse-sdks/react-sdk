import { createUseStyles } from "react-jss";

export const useLablebLogoStyles = createUseStyles({
  "logoAnchor": {
    display: "flex",
    justifyItems: "center",
    alignItems: "center"
  },
  "logo": {
    width: "30px",
    height: "30px",
    maxWidth: "30px",
    maxHeight: "30px",
    "&:hover": { cursor: "pointer" },
  },
  "poweredByContainer": {
    display: "flex",
    justifyContent: "flex-end",
    alignItems: "center",
    "&>span": {
      color: "var(--main-color)",
      padding: "0 5px",
      fontSize: "0.8rem"
    }
  },
});
