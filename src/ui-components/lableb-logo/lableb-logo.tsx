import React from 'react';
import logo from '../../assets/images/logo.svg';
import { translations } from '../search-box/search.translation';
import { LablebLogoProps } from './lableb-logo.type';
import { useLablebLogoStyles } from './lableb-logo.style';


export function LablebLogo(props: LablebLogoProps) {

    const { language, poweredBy } = props;
    const classes = useLablebLogoStyles();

    const Logo = (
        <a
            className={classes.logoAnchor}
            href="https://lableb.com"
            target="_blank"
        >
            <img
                src={logo}
                alt="Lableb"
                className={classes.logo}
                title={translations[language]["LABLEB_CLOUD_SEARCH"]}
            />
        </a>
    );

    if (!poweredBy)
        return (
            <>
                {Logo}
            </>
        );

    return (
        <div className={classes.poweredByContainer}>
            <span>
                {translations[language]['POWERED_BY']}
            </span>
            {Logo}
        </div>
    );

}

