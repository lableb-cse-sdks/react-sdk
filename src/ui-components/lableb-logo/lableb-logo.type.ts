import { Language } from '../../types/main';


export interface LablebLogoProps {
    language: Language,
    poweredBy?: boolean,
}