export interface BucketObject { value: string, count: number }
export type Language = 'en' | 'ar';

export interface LablebSearchFacets {
    [facetName: string]: {
        buckets: BucketObject[]
    }
}
