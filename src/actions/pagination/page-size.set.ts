import { SDKActionCreator } from "../../types/actions";

export const SET_PAGE_SIZE = 'SET_PAGE_SIZE';

export interface SetPageSizeAction {
    type: typeof SET_PAGE_SIZE,
    pageSize: number,
}

export type SetPageSizeActionCreator = SDKActionCreator<SetPageSizeAction>;

export const setPageSize: SetPageSizeActionCreator =
    (params) =>
    ({
        type: SET_PAGE_SIZE,
        ...params,
    });