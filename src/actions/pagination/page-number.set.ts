import { SDKActionCreator } from "../../types/actions";

export const SET_PAGE_NUMBER = 'SET_PAGE_NUMBER';

export interface SetPageNumberAction {
    type: typeof SET_PAGE_NUMBER,
    pageNumber: number,
}

export type SetPageNumberActionCreator = SDKActionCreator<SetPageNumberAction>;

export const setPageNumber: SetPageNumberActionCreator =
    (params) =>
    ({
        type: SET_PAGE_NUMBER,
        ...params,
    });