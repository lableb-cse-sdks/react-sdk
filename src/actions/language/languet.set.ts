import { SDKActionCreator } from "../../types/actions";
import { Language } from "../../types/main";

export const SET_LANGUAGE = 'SET_LANGUAGE';

export interface SetLanguageAction {
    type: typeof SET_LANGUAGE,
    language: Language
}

export type SetLanguageActionCreator =
    SDKActionCreator<SetLanguageAction>;

export const setLanguage: SetLanguageActionCreator =
    (params) =>
    ({
        type: SET_LANGUAGE,
        ...params
    });