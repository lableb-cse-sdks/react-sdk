import { SDKActionCreator } from "../../types/actions";

export const INCREMENT_LOADING_STATE = 'INCREMENT_LOADING_STATE';

export interface IncrementLoadingStateAction {
    type: typeof INCREMENT_LOADING_STATE
}

export type IncrementLoadingStateActionCreator = SDKActionCreator<IncrementLoadingStateAction>;

export const incrementLoadingState: IncrementLoadingStateActionCreator =
    (params) =>
    ({
        type: INCREMENT_LOADING_STATE,
        ...params,
    })