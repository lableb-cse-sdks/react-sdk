import { SDKActionCreator } from "../../types/actions";

export const DECREMENT_LOADING_STATE = 'DECREMENT_LOADING_STATE';

export interface DecrementLoadingStateAction {
    type: typeof DECREMENT_LOADING_STATE
}

export type DecrementLoadingStateActionCreator = SDKActionCreator<DecrementLoadingStateAction>;

export const decrementLoadingState: DecrementLoadingStateActionCreator =
    (params) =>
    ({
        type: DECREMENT_LOADING_STATE,
        ...params,
    })