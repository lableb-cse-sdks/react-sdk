import { LablebClient } from "@lableb/javascript-sdk";
import { SDKActionCreator } from "../../types/actions";
import { UnPromise } from "../../types/utility";


export const SAVE_LABLEB_CLIENT = 'SAVE_LABLEB_CLIENT';


export interface SaveLablebClientAction {
    type: typeof SAVE_LABLEB_CLIENT,
    lablebClient: UnPromise<ReturnType<typeof LablebClient>>,
}


export type SaveLablebClientActionCreator = SDKActionCreator<SaveLablebClientAction>;


export const saveLablebClient: SaveLablebClientActionCreator =
    (params) =>
    ({
        type: SAVE_LABLEB_CLIENT,
        ...params
    });
