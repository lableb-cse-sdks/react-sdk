import { LablebClientOptions } from "@lableb/javascript-sdk";
import { SDKActionCreator } from "../../types/actions";


export const SET_PLATFORM_OPTIONS = 'SET_PLATFORM_OPTIONS';


export interface SetPlatformOptionsAction {
    type: typeof SET_PLATFORM_OPTIONS,
    platformOptions: LablebClientOptions
}


export type SetPlatformOptionsActionCreator = SDKActionCreator<SetPlatformOptionsAction>;


export const setPlatformOptions: SetPlatformOptionsActionCreator =
    (params) =>
    ({
        type: SET_PLATFORM_OPTIONS,
        ...params
    });
