import { LablebClientAutocompleteResponse } from '@lableb/javascript-sdk';
import { SDKActionCreator } from '../../types/actions';

export const SET_AUTOCOMPLETE_RESULTS = 'SET_AUTOCOMPLETE_RESULTS';

export interface SetAutocompleteResultsAction {
    type: typeof SET_AUTOCOMPLETE_RESULTS,
    autocompleteResponse: LablebClientAutocompleteResponse,
}

export type SetAutocompleteResultsActionCreator = SDKActionCreator<SetAutocompleteResultsAction>;

export const setAutocompleteResults: SetAutocompleteResultsActionCreator =
    (params) =>
    ({
        type: SET_AUTOCOMPLETE_RESULTS,
        ...params,
    });
