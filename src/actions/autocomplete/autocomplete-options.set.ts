import { LablebAutocompleteOptions } from "@lableb/javascript-sdk";
import { SDKActionCreator } from "../../types/actions";

export const SET_AUTOCOMPLETE_OPTIONS = 'SET_AUTOCOMPLETE_OPTIONS';

export interface SetAutocompleteOptionsAction extends Partial<Omit<LablebAutocompleteOptions, "query">> {
    type: typeof SET_AUTOCOMPLETE_OPTIONS,
}

export type SetAutocompleteOptionsActionCreator = SDKActionCreator<SetAutocompleteOptionsAction>;

export const setAutocompleteOptions: SetAutocompleteOptionsActionCreator =
    (params) =>
    ({
        type: SET_AUTOCOMPLETE_OPTIONS,
        ...params,
    });