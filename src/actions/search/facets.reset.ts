import { SDKActionCreator } from "../../types/actions";

export const RESET_FACET = 'RESET_FACET';

export interface ResetFacetsAction {
    type: typeof RESET_FACET,
}

export type ResetFacetsActionCreator = SDKActionCreator<ResetFacetsAction>;

export const resetFacets: ResetFacetsActionCreator =
    (params) =>
    ({
        type: RESET_FACET,
        ...params,
    });