import { LablebClientSearchResponse } from '@lableb/javascript-sdk';
import { SDKActionCreator } from '../../types/actions';

export const SET_SEARCH_RESULTS = 'SET_SEARCH_RESULTS';

export interface SetSearchResultsAction {
    type: typeof SET_SEARCH_RESULTS,
    searchResponse: LablebClientSearchResponse,
}

export type SetSearchResultsActionCreator = SDKActionCreator<SetSearchResultsAction>;

export const setSearchResults: SetSearchResultsActionCreator =
    (params) =>
    ({
        type: SET_SEARCH_RESULTS,
        ...params,
    });
