import { LablebSearchOptions } from "@lableb/javascript-sdk";
import { SDKActionCreator } from "../../types/actions";

export const SET_SEARCH_OPTIONS = 'SET_SEARCH_OPTIONS';


export interface SetSearchOptionsAction extends Partial<LablebSearchOptions> {
    type: typeof SET_SEARCH_OPTIONS,
}

export type SetSearchOptionsActionCreator = SDKActionCreator<SetSearchOptionsAction>;


export const setSearchOptions: SetSearchOptionsActionCreator =
    (params) =>
    ({
        type: SET_SEARCH_OPTIONS,
        ...params,
    });