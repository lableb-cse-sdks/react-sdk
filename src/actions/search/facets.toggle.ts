import { SDKActionCreator } from "../../types/actions";

export const TOGGLE_FACET = 'TOGGLE_FACET';

export interface ToggleFacetAction {
    type: typeof TOGGLE_FACET,

    facetName: string,
    facetValue: string,
}

export type ToggleFacetActionCreator = SDKActionCreator<ToggleFacetAction>;

export const toggleFacet: ToggleFacetActionCreator =
    (params) =>
    ({
        type: TOGGLE_FACET,
        ...params,
    });