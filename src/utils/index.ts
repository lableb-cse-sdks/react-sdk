import { globalStateReducer } from "../state/state.reducer";

export const inverse = (x: boolean) => !x;
export const sum = (x: number, y: number) => x + y;

export const isLablebClientReady = (state: ReturnType<typeof globalStateReducer>) =>
    new Promise((res, rej) => {
        if (state.client && state.client.search)
            res(true);
        else
            rej('Client not ready yet');
    });