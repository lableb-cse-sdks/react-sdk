import {
    LablebClient,
    LablebClientAutocompleteResponse,
    LablebClientOptions,
    LablebClientSearchResponse,
    SelectedFacets,
} from "@lableb/javascript-sdk";
import { SetAutocompleteResultsAction } from "../actions/autocomplete/autocomplete-results.set";
import { SaveLablebClientAction } from "../actions/lableb-client/lableb-client.save";
import { SetPlatformOptionsAction } from "../actions/lableb-client/platform.options";
import { SetLanguageAction } from "../actions/language/languet.set";
import { DecrementLoadingStateAction } from "../actions/loading/decrement-loading";
import { IncrementLoadingStateAction } from "../actions/loading/increment-loading";
import { SetPageNumberAction } from "../actions/pagination/page-number.set";
import { SetPageSizeAction } from "../actions/pagination/page-size.set";
import { ResetFacetsAction } from "../actions/search/facets.reset";
import { ToggleFacetAction } from "../actions/search/facets.toggle";
import { SetSearchResultsAction } from "../actions/search/search-results.set";
import { Language } from "../types/main";
import { UnPromise } from "../types/utility";


export interface GlobalState {
    client: UnPromise<ReturnType<typeof LablebClient>> | undefined,
    platformOptions: LablebClientOptions,
    facets: SelectedFacets,
    searchResults: LablebClientSearchResponse,
    autocompleteResults: LablebClientAutocompleteResponse,
    pageNumber: number,
    pageSize: number,
    language: Language,
    pendingRequests: number,
}


export type GlobalActions =
    SetPlatformOptionsAction |
    SaveLablebClientAction |
    ToggleFacetAction |
    SetSearchResultsAction |
    SetAutocompleteResultsAction |
    ResetFacetsAction |
    SetPageNumberAction |
    SetPageSizeAction |
    SetLanguageAction |
    IncrementLoadingStateAction |
    DecrementLoadingStateAction;

