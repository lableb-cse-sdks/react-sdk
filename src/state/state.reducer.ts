import produce from "immer";
import { SET_AUTOCOMPLETE_RESULTS } from "../actions/autocomplete/autocomplete-results.set";
import { SAVE_LABLEB_CLIENT } from "../actions/lableb-client/lableb-client.save";
import { SET_PLATFORM_OPTIONS } from "../actions/lableb-client/platform.options";
import { SET_LANGUAGE } from "../actions/language/languet.set";
import { DECREMENT_LOADING_STATE } from "../actions/loading/decrement-loading";
import { INCREMENT_LOADING_STATE } from "../actions/loading/increment-loading";
import { SET_PAGE_NUMBER } from "../actions/pagination/page-number.set";
import { SET_PAGE_SIZE } from "../actions/pagination/page-size.set";
import { RESET_FACET } from "../actions/search/facets.reset";
import { TOGGLE_FACET } from "../actions/search/facets.toggle";
import { SET_SEARCH_RESULTS } from "../actions/search/search-results.set";
import { GLOBAL_STATE_DEFAULT } from "./state.context";
import { GlobalActions, GlobalState } from "./state.type";



export const globalStateReducer = produce(
    function globalStateReducer(state: GlobalState, action: GlobalActions) {


        switch (action.type) {

            case SET_LANGUAGE: {
                state.language = action.language;
                break;
            }

            case SET_PAGE_NUMBER: {
                if (action.pageNumber >= 0)
                    state.pageNumber = action.pageNumber;
                break;
            }

            case SET_PAGE_SIZE: {
                if (action.pageSize > 0)
                    state.pageSize = action.pageSize;
                break;
            }

            case SET_PLATFORM_OPTIONS: {
                state.platformOptions = action.platformOptions;
                break;
            }

            case SAVE_LABLEB_CLIENT: {
                state.client = action.lablebClient;
                break;
            }


            case RESET_FACET: {

                state.facets = {}
                state.pageNumber = 0;
                break;
            }


            case SET_SEARCH_RESULTS: {
                state.searchResults = action.searchResponse;
                break;
            }


            case SET_AUTOCOMPLETE_RESULTS: {

                state.autocompleteResults = action.autocompleteResponse;
                break;
            }

            case INCREMENT_LOADING_STATE: {
                state.pendingRequests = state.pendingRequests + 1;
                break;
            }

            case DECREMENT_LOADING_STATE: {
                state.pendingRequests = Math.max(0, state.pendingRequests - 1);
                break;
            }

            case TOGGLE_FACET: {

                state.pageNumber = 0;

                const { type, facetName, facetValue } = action;

                const currentFacets = { ...state?.facets };

                const facetNameAlreadyExist = Boolean(currentFacets[facetName]);

                if (!facetNameAlreadyExist) {

                    currentFacets[facetName] = [facetValue]

                } else {

                    const facetValueExistBefore = Boolean(currentFacets[facetName].find(value => value == facetValue));

                    if (facetValueExistBefore) {
                        currentFacets[facetName] = currentFacets[facetName].filter(value => value != facetValue);
                        if (!currentFacets[facetName].length)
                            delete currentFacets[facetName];
                    }
                    else
                        currentFacets[facetName] = currentFacets[facetName].concat([facetValue]);
                }

                state.facets = currentFacets;
                break;
            }


            default:
                return state;
        }
    },
    GLOBAL_STATE_DEFAULT
);