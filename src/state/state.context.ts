import { LablebClient } from "@lableb/javascript-sdk";
import { createContext } from "react";
import { DEFAULT_AUTOCOMPLETE_RESULTS, DEFAULT_SEARCH_RESULTS } from "../config/defaults";
import { GlobalActions, GlobalState } from "./state.type";


export interface GlobalStateContextType {
    state: GlobalState,
    dispatch: (action: GlobalActions) => void,
}

export const GLOBAL_STATE_DEFAULT: GlobalState = {
    client: undefined,
    platformOptions: {},
    facets: {},
    pageNumber: 0,
    pageSize: 14,
    searchResults: DEFAULT_SEARCH_RESULTS,
    autocompleteResults: DEFAULT_AUTOCOMPLETE_RESULTS,
    language: 'en',
    pendingRequests: 0,
}

export const GlobalStateContext = createContext<GlobalStateContextType>({
    state: GLOBAL_STATE_DEFAULT,
    dispatch: (action: GlobalActions) => { }
});

