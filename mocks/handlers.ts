import { rest } from 'msw'
import {
    AutocompleteRequestResult,
    BatchAutocompleteFeedbackRequestResult,
    BatchRecommendFeedbackRequestResult,
    BatchSearchFeedbackRequestResult,
    DeleteRequestResult,
    IndexingRequestResult,
    RecommendRequestResult,
    SearchRequestResult
} from '@lableb/javascript-sdk';
import { MOCK_BACKEND_API } from './manual.api.mock';
import { extractDataAndAuthenticate } from './mock.extractor';



export const handlers = [

    rest.get(MOCK_BACKEND_API.SEARCH_URL, (req, res, ctx) => {

        try {
            const params: BatchSearchFeedbackRequestResult['params'] = extractDataAndAuthenticate(req, res, ctx);

            const RESPONSE = MOCK_BACKEND_API.search({
                params: params as SearchRequestResult['params'],
            });

            return res(
                ctx.status(200),
                ctx.json({
                    response: RESPONSE,
                    code: 200,
                    time: 141,
                })
            );

        } catch (error) {
            return res(
                ctx.status(401),
            );
        }

    }),



    rest.get(MOCK_BACKEND_API.AUTOCOMPLETE_URL, (req, res, ctx) => {

        try {
            const params: AutocompleteRequestResult['params'] = extractDataAndAuthenticate(req, res, ctx);


            const RESPONSE = MOCK_BACKEND_API.autocomplete({
                params: params as AutocompleteRequestResult['params'],
            });

            return res(
                ctx.status(200),
                ctx.json({
                    response: RESPONSE,
                    code: 200,
                    time: 141,
                })
            );

        } catch (error) {
            return res(
                ctx.status(401),
            );
        }

    }),


    rest.get(MOCK_BACKEND_API.RECOMMEND_URL, (req, res, ctx) => {

        try {
            const params: RecommendRequestResult['params'] = extractDataAndAuthenticate(req, res, ctx);


            const RESPONSE = MOCK_BACKEND_API.recommend({
                params: params as RecommendRequestResult['params'],
            });

            return res(
                ctx.status(200),
                ctx.json({
                    response: RESPONSE,
                    code: 200,
                    time: 141,
                })
            );

        } catch (error) {
            return res(
                ctx.status(401),
            );
        }

    }),


    rest.post(MOCK_BACKEND_API.INDEX_URL, (req, res, ctx) => {

        try {
            const params: IndexingRequestResult['params'] = extractDataAndAuthenticate(req, res, ctx);


            const RESPONSE = MOCK_BACKEND_API.index({
                params: params as IndexingRequestResult['params'],
            });

            return res(
                ctx.status(200),
                ctx.json({
                    response: RESPONSE,
                    code: 200,
                    time: 141,
                })
            );

        } catch (error) {
            return res(
                ctx.status(401),
            );
        }

    }),

    rest.delete(MOCK_BACKEND_API.DELETE_URL, (req, res, ctx) => {

        try {
            const params: DeleteRequestResult['params'] = extractDataAndAuthenticate(req, res, ctx);


            const RESPONSE = MOCK_BACKEND_API.delete({
                params: params as DeleteRequestResult['params'],
            });

            return res(
                ctx.status(200),
                ctx.json({
                    response: RESPONSE,
                    code: 200,
                    time: 141,
                })
            );

        } catch (error) {
            return res(
                ctx.status(401),
            );
        }
    }),



    rest.post(MOCK_BACKEND_API.SEARCH_FEEDBACK_URL, (req, res, ctx) => {

        try {
            const params: BatchSearchFeedbackRequestResult['params'] = extractDataAndAuthenticate(req, res, ctx);


            const RESPONSE = MOCK_BACKEND_API.searchFeedback({
                params: params as BatchSearchFeedbackRequestResult['params'],
            });

            return res(
                ctx.status(200),
                ctx.json({
                    response: RESPONSE,
                    code: 200,
                    time: 141,
                })
            );

        } catch (error) {
            return res(
                ctx.status(401),
            );
        }
    }),



    rest.post(MOCK_BACKEND_API.AUTOCOMPLETE_FEEDBACK_URL, (req, res, ctx) => {

        try {
            const params: BatchAutocompleteFeedbackRequestResult['params'] = extractDataAndAuthenticate(req, res, ctx);


            const RESPONSE = MOCK_BACKEND_API.autocompleteFeedback({
                params: params as BatchAutocompleteFeedbackRequestResult['params'],
            });

            return res(
                ctx.status(200),
                ctx.json({
                    response: RESPONSE,
                    code: 200,
                    time: 141,
                })
            );

        } catch (error) {
            return res(
                ctx.status(401),
            );
        }
    }),



    rest.post(MOCK_BACKEND_API.RECOMMEND_FEEDBACK_URL, (req, res, ctx) => {

        try {
            const params: BatchRecommendFeedbackRequestResult['params'] = extractDataAndAuthenticate(req, res, ctx);


            const RESPONSE = MOCK_BACKEND_API.recommendFeedback({
                params: params as BatchRecommendFeedbackRequestResult['params'],
            });

            return res(
                ctx.status(200),
                ctx.json({
                    response: RESPONSE,
                    code: 200,
                    time: 141,
                })
            );

        } catch (error) {
            return res(
                ctx.status(401),
            );
        }
    }),


    rest.delete('http://localhost/test-delete', (req, res, ctx) => {

        return res(
            ctx.status(200),
            ctx.json({
                text: "hello world"
            })
        );
    }),

    rest.get('http://localhost/test-get', (req, res, ctx) => {

        return res(
            ctx.status(200),
            ctx.json({
                text: "hello world"
            })
        );
    }),


    rest.post('http://localhost/test-post', (req, res, ctx) => {

        return res(
            ctx.status(200),
            ctx.json({
                text: "hello world"
            })
        );
    }),

    rest.put('http://localhost/test-put', (req, res, ctx) => {

        return res(
            ctx.status(200),
            ctx.json({
                text: "hello world"
            })
        );
    }),
]