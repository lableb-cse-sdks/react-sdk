import {
    AutocompleteRequestResult,
    SearchRequestResult,
    LablebClientAutocompleteResponse,
    LablebClientSearchResponse,
    RecommendRequestResult,
    LablebClientIndexingResponse,
    LablebClientDeleteResponse,
    LablebClientBatchSearchFeedbackResponse,
    LablebClientBatchAutocompleteFeedbackResponse,
    LablebClientBatchRecommendFeedbackResponse,
    LablebClientRecommendResponse,
} from "@lableb/javascript-sdk";


const searchExampleResponse = require('./search.example.json');


export const MOCK_BACKEND_API = {

    SEARCH_URL: `${process.env.API_BASE_URL}/projects/${process.env.PLATFORM_NAME}/indices/${process.env.GLOBAL_DEFAULT_INDEX_NAME}/search/${process.env.GLOBAL_DEFAULT_SEARCH_HANDLER}`,

    search: function _search({
        params,
    }: {
        params: SearchRequestResult['params'],
    }): LablebClientSearchResponse {

        return searchExampleResponse.response;
    },



    AUTOCOMPLETE_URL: `${process.env.API_BASE_URL}/projects/${process.env.PLATFORM_NAME}/indices/${process.env.GLOBAL_DEFAULT_INDEX_NAME}/autocomplete/${process.env.GLOBAL_DEFAULT_AUTOCOMPLETE_HANDLER}`,

    autocomplete: function _autocomplete({
        params,
    }: {
        params: AutocompleteRequestResult['params'],
    }): LablebClientAutocompleteResponse {


        return searchExampleResponse.response;
    },


    RECOMMEND_URL: `${process.env.API_BASE_URL}/projects/${process.env.PLATFORM_NAME}/indices/${process.env.GLOBAL_DEFAULT_INDEX_NAME}/recommend/${process.env.GLOBAL_DEFAULT_RECOMMEND_HANDLER}`,

    recommend: function _recommend({
        params,
    }: {
        params: RecommendRequestResult['params'],
    }): LablebClientRecommendResponse {


        return searchExampleResponse.response;
    },


    INDEX_URL: `${process.env.API_BASE_URL}/projects/${process.env.PLATFORM_NAME}/indices/${process.env.GLOBAL_DEFAULT_INDEX_NAME}/documents`,

    index: function _index({
        params,
    }: {
        params: any
    }): LablebClientIndexingResponse {


        return null;
    },


    DELETE_URL: `${process.env.API_BASE_URL}/projects/${process.env.PLATFORM_NAME}/indices/${process.env.GLOBAL_DEFAULT_INDEX_NAME}/documents/1`,

    delete: function _delete({
        params,
    }: {
        params: any
    }): LablebClientDeleteResponse {


        return null;
    },


    SEARCH_FEEDBACK_URL: `${process.env.API_BASE_URL}/projects/${process.env.PLATFORM_NAME}/indices/${process.env.GLOBAL_DEFAULT_INDEX_NAME}/search/${process.env.GLOBAL_DEFAULT_SEARCH_HANDLER}/feedback/hits`,

    searchFeedback: function _searchFeedback({
        params,
    }: {
        params: any

    }): LablebClientBatchSearchFeedbackResponse {


        return null;
    },


    AUTOCOMPLETE_FEEDBACK_URL: `${process.env.API_BASE_URL}/projects/${process.env.PLATFORM_NAME}/indices/${process.env.GLOBAL_DEFAULT_INDEX_NAME}/autocomplete/${process.env.GLOBAL_DEFAULT_AUTOCOMPLETE_HANDLER}/feedback/hits`,

    autocompleteFeedback: function _autocompleteFeedback({
        params,
    }: {
        params: any
    }): LablebClientBatchAutocompleteFeedbackResponse {


        return null;
    },



    RECOMMEND_FEEDBACK_URL: `${process.env.API_BASE_URL}/projects/${process.env.PLATFORM_NAME}/indices/${process.env.GLOBAL_DEFAULT_INDEX_NAME}/recommend/${process.env.GLOBAL_DEFAULT_RECOMMEND_HANDLER}/feedback/hits`,

    recommendFeedback: function _recommendFeedback({
        params,
    }: {
        params: any
    }): LablebClientBatchRecommendFeedbackResponse {


        return null;
    },
}
