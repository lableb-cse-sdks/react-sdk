
export function extractDataAndAuthenticate<T>(req: any, res: any, ctx: any) {

    const jwtToken = req.headers.get('authorization')?.replace('Bearer ', '');

    const params: any = req.url.searchParams.toString().split('&').reduce((prev: any, next: any) => {
        return {
            ...prev,
            [next.split('=')[0]]: next.split('=')[1]
        }
    }, {});


    if (
        (params.apikey && params.apikey != process.env.API_KEY && params.apikey != process.env.INDEX_API_KEY)
        ||
        (!params.apikey && jwtToken && jwtToken != process.env.GLOBAL_TEST_JWT)
        ||
        (!params.apikey && !jwtToken)
    )
        throw new Error()

    return params as T;

}