# Lableb React SDK

## Add Great Search user experience

Easy, Small, configurable UI library that allow instant integration with [Lableb](https://lableb.com)

---------------------

## Install

```sh
yarn add @lableb/react-sdk # or: npm i @lableb/react-sdk
```

### Usage Example in SSR Project

> Note: For SSR techniques, like [next.js](https://nextjs.org/docs/messages/invalid-resolve-alias) you have to alias `react` and `react-dom` in your configuration because they are used as `peer-dependencies` in this library

```js
webpack(config) {

	config.resolve.alias = {
		...config.resolve.alias,
		'react': path.resolve(__dirname, './node_modules/react'),
		'react-dom': path.resolve(__dirname, './node_modules/react-dom'),
	}

	return config;
}
```


----------------


## Setup

Wrap all Lableb components(or your components tree) with `LablebProvider`

```js

import { LablebProvider } from '@lableb/react-sdk';


export function Root(){

	return (
		<LablebProvider
			platformOptions={{
				APIKey: API_KEY,
				platformName: PLATFORM_NAME,
			}}
		>

		...

		</LablebProvider>
	);

}
```

Then import the needed component into your components tree, normally you'll need a search bar and a filters and your are done.

```js
import { LablebSearch, LablebSearchFilters } from '@lableb/react-sdk';

...


export function Root(){

	return (
		<LablebProvider
			platformOptions={{
				APIKey: API_KEY,
				platformName: PLATFORM_NAME,
			}}
		>

			<LablebSearch />

			<LablebSearchFilters />

		</LablebProvider>
	);

}
```

--------------------


## Lableb Components

Lableb SDK export many useful UI components that can cover all UX needs
  
  - Search box with autocomplete and embedded filters

	Search components render a search box with autocomplete and filters(optional), search and autocomplete results are fetched automatically after some delay.

	You can controls what the search bar contains, or render your own UI with render props as a children of the component.

	```js
	<LablebProvider
		platformOptions={{
			platformName: 'PLATFORM_NAME',
			APIKey: 'API_KEY'
		}}
	>
		<LablebSearch />
	</LablebProvider>
	```

	[Lableb Search API](https://gitlab.com/lableb-cse-sdks/react-sdk/-/tree/master/documentation/search.md)

  - Search results

	Display search results for the end users as cards, with the option to show related documentations or render your own UI with render props patter.

	```js
	<LablebProvider
		platformOptions={{
			platformName: 'PLATFORM_NAME',
			APIKey: 'API_KEY',
		}}
	>
		<LablebSearch />
		<LablebSearchResults />
	</LablebProvider>
	```

	[Lableb Search Results API](https://gitlab.com/lableb-cse-sdks/react-sdk/-/tree/master/documentation/search-results.md)

  - Search Filters

	Display a complete list of search filters UI with different display options

	```js
	<LablebProvider
		platformOptions={{
			platformName: 'PLATFORM_NAME',
			APIKey: 'API_KEY',
		}}
	>

		<LablebSearchFilters />

	</LablebProvider>
	```

	[Lableb Search Filters API](https://gitlab.com/lableb-cse-sdks/react-sdk/-/tree/master/documentation/search-filters.md)

  - Recommendations

	Display recommendations results of a given document

	```js
	<LablebProvider
		platformOptions={{
			platformName: 'PLATFORM_NAME',
			APIKey: 'API_KEY',
		}}
	>

		<LablebSearch />

		<LablebRecommend
			id={3}
		/>

	</LablebProvider>
	```

	[Lableb Recommendation API](https://gitlab.com/lableb-cse-sdks/react-sdk/-/tree/master/documentation/recommend.md)

  - Pagination

	You can use `LablebPagination` component to easily paginate your search results

	```js
	<LablebProvider
		platformOptions={{
			APIKey: process.env.API_KEY,
			platformName: process.env.PLATFORM_NAME,
		}}
	>
		
		<LablebSearch />

		<LablebPagination pageSize={9} />

	</LablebProvider>
	```

	[Lableb Pagination API](https://gitlab.com/lableb-cse-sdks/react-sdk/-/tree/master/documentation/pagination.md)


Each component can be 
  
- Extended: using render props to completely control how you render your UI
- Configurable: pass the right options to make your search experience more suitable to you


--------------

# Examples

Each component has a mini example in documentation, however for a fully functional example of all Lableb components working together, please check the [/examples](https://gitlab.com/lableb-cse-sdks/react-sdk/-/tree/master/examples) folder.


Example overview:

  - simple-start
	render Lableb's components without any customization
  - use-custom-ui
	render customized UI components, depending on Lableb's components render props to provide the right state and functions