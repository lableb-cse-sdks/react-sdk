export declare const DEFAULT_SEARCH_RESULTS: {
    facets: {};
    found_documents: number;
    results: never[];
};
export declare const DEFAULT_AUTOCOMPLETE_RESULTS: {
    facets: {};
    found_documents: number;
    results: never[];
};
