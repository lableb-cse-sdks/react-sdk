export declare const usePaginationStyle: (data?: {
    theme?: Jss.Theme | undefined;
} | undefined) => import("jss").Classes<"container" | "pageButton" | "pageButton:hover" | "selectedPage" | "reverseImage">;
