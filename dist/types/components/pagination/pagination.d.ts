import React from "react";
import { LablebPaginationContainer } from "./pagination.container";
export declare function LablebPagination(props: React.ComponentProps<typeof LablebPaginationContainer>): JSX.Element;
