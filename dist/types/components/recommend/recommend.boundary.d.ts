import React from 'react';
export declare class LablebRecommendErrorBoundary extends React.Component<any, any> {
    constructor(props: any);
    static getDerivedStateFromError(error: any): {
        hasError: boolean;
    };
    componentDidCatch(error: any, errorInfo: any): void;
    render(): React.ReactNode;
}
