import { OverrideSearchOptions } from "./search.type";
export declare function useLablebSearch({ query, overrideSearchOptions, preventAutoSearch, }: {
    query: string;
    overrideSearchOptions?: OverrideSearchOptions;
    preventAutoSearch?: boolean;
}): {
    SearchAtLableb: () => void;
};
