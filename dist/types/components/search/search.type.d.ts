import { LablebAutocompleteOptions, LablebClientAutocompleteResponse, LablebDocumentWithFeedback, LablebSearchOptions } from "@lableb/javascript-sdk";
import { ReactNode } from "react";
export declare type OverrideSearchOptions = Omit<LablebSearchOptions, "query" | "searchHandler" | "facets">;
export declare type OverrideAutocompleteOptions = Omit<LablebAutocompleteOptions, "query" | "autocompleteHandler">;
export interface SearchRenderProps {
    query: string;
    setQuery: (query: string) => void;
    autocompleteResults: LablebClientAutocompleteResponse['results'];
    openAutocompleteResults: boolean;
    setOpenAutocompleteResults: (value: boolean) => void;
    sendAutocompleteFeedback: (document: LablebDocumentWithFeedback) => void;
    searchAtLableb: () => void;
    loading: boolean;
}
export interface LablebSearchProps {
    searchOptions?: OverrideSearchOptions;
    autocompleteOptions?: OverrideAutocompleteOptions;
    children?: (props: SearchRenderProps) => ReactNode;
    inlineLogo?: boolean;
    inlineFilters?: boolean;
    preventAutoSearch?: boolean;
    defaultQuery?: string;
}
