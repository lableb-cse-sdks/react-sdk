export declare const useLablebSearchStyles: (data?: {
    theme?: Jss.Theme | undefined;
} | undefined) => import("jss").Classes<"searchBoxContainer">;
