import { OverrideAutocompleteOptions } from "./search.type";
export declare function useLablebAutocomplete({ query, overrideAutocompleteOptions, innerFilters }: {
    query: string;
    overrideAutocompleteOptions?: OverrideAutocompleteOptions;
    innerFilters: string;
}): void;
