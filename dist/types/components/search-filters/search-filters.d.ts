import React from "react";
import { LablebSearchFiltersUI } from "./search-filters.ui";
export declare function LablebSearchFilters(props: React.ComponentProps<typeof LablebSearchFiltersUI>): JSX.Element;
