import { SelectedFacets } from "@lableb/javascript-sdk";
export declare function useDefaultFacets({ defaultFacets }: {
    defaultFacets?: SelectedFacets;
}): void;
