import { LablebClientSearchResponse, SelectedFacets } from "@lableb/javascript-sdk";
export declare function mergeResponseFacetsWithInputFacets({ inputFacets, responseFacets, }: {
    inputFacets: SelectedFacets;
    responseFacets: LablebClientSearchResponse['facets'];
}): import("@lableb/javascript-sdk/dist/types/core/search/search.type").LablebFacets;
