import React from "react";
import { LablebSearchFilters } from './search-filters';
export declare function VirtualSearchFilters(props: React.ComponentProps<typeof LablebSearchFilters>): JSX.Element;
