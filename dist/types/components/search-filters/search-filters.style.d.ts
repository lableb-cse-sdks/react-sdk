export declare const useSearchFiltersStyles: (data?: {
    theme?: Jss.Theme | undefined;
} | undefined) => import("jss").Classes<"searchFacetsContainer" | "searchFacetsHead">;
