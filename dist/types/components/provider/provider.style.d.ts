export declare const useProviderStyles: (data?: {
    theme?: Jss.Theme | undefined;
} | undefined) => import("jss").Classes<"@global" | "lablebRoot" | "lablebRootAr">;
