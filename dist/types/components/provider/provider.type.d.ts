import { LablebClientOptions } from '@lableb/javascript-sdk';
import { CSSProperties, ReactNode } from 'react';
export interface LablebProviderProps {
    platformOptions: LablebClientOptions & Required<{
        platformName: string;
        APIKey: string;
    }>;
    layoutDirection?: 'RTL' | 'LTR';
    children?: ReactNode;
    style?: CSSProperties;
}
