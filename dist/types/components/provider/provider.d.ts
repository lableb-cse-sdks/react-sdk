/// <reference types="react" />
import { LablebProviderProps } from "./provider.type";
export declare function LablebProvider(props: LablebProviderProps): JSX.Element;
