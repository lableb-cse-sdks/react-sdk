export declare const useSearchResultsStyles: (data?: {
    theme?: Jss.Theme | undefined;
} | undefined) => import("jss").Classes<"dialogHeadText">;
