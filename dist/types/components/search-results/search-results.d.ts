import React from "react";
import { SearchResultsUI } from "./search-results.ui";
export declare function LablebSearchResults(props: React.ComponentProps<typeof SearchResultsUI>): JSX.Element;
