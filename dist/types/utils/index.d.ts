import { globalStateReducer } from "../state/state.reducer";
export declare const inverse: (x: boolean) => boolean;
export declare const sum: (x: number, y: number) => number;
export declare const isLablebClientReady: (state: ReturnType<typeof globalStateReducer>) => Promise<unknown>;
