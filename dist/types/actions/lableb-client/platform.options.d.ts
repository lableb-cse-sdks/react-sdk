import { LablebClientOptions } from "@lableb/javascript-sdk";
import { SDKActionCreator } from "../../types/actions";
export declare const SET_PLATFORM_OPTIONS = "SET_PLATFORM_OPTIONS";
export interface SetPlatformOptionsAction {
    type: typeof SET_PLATFORM_OPTIONS;
    platformOptions: LablebClientOptions;
}
export declare type SetPlatformOptionsActionCreator = SDKActionCreator<SetPlatformOptionsAction>;
export declare const setPlatformOptions: SetPlatformOptionsActionCreator;
