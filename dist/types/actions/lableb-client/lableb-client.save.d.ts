import { LablebClient } from "@lableb/javascript-sdk";
import { SDKActionCreator } from "../../types/actions";
import { UnPromise } from "../../types/utility";
export declare const SAVE_LABLEB_CLIENT = "SAVE_LABLEB_CLIENT";
export interface SaveLablebClientAction {
    type: typeof SAVE_LABLEB_CLIENT;
    lablebClient: UnPromise<ReturnType<typeof LablebClient>>;
}
export declare type SaveLablebClientActionCreator = SDKActionCreator<SaveLablebClientAction>;
export declare const saveLablebClient: SaveLablebClientActionCreator;
