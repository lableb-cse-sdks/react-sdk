import { LablebClientAutocompleteResponse } from '@lableb/javascript-sdk';
import { SDKActionCreator } from '../../types/actions';
export declare const SET_AUTOCOMPLETE_RESULTS = "SET_AUTOCOMPLETE_RESULTS";
export interface SetAutocompleteResultsAction {
    type: typeof SET_AUTOCOMPLETE_RESULTS;
    autocompleteResponse: LablebClientAutocompleteResponse;
}
export declare type SetAutocompleteResultsActionCreator = SDKActionCreator<SetAutocompleteResultsAction>;
export declare const setAutocompleteResults: SetAutocompleteResultsActionCreator;
