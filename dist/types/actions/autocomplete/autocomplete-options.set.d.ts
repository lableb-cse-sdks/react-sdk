import { LablebAutocompleteOptions } from "@lableb/javascript-sdk";
import { SDKActionCreator } from "../../types/actions";
export declare const SET_AUTOCOMPLETE_OPTIONS = "SET_AUTOCOMPLETE_OPTIONS";
export interface SetAutocompleteOptionsAction extends Partial<Omit<LablebAutocompleteOptions, "query">> {
    type: typeof SET_AUTOCOMPLETE_OPTIONS;
}
export declare type SetAutocompleteOptionsActionCreator = SDKActionCreator<SetAutocompleteOptionsAction>;
export declare const setAutocompleteOptions: SetAutocompleteOptionsActionCreator;
