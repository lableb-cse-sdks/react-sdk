import { SDKActionCreator } from "../../types/actions";
export declare const SET_PAGE_NUMBER = "SET_PAGE_NUMBER";
export interface SetPageNumberAction {
    type: typeof SET_PAGE_NUMBER;
    pageNumber: number;
}
export declare type SetPageNumberActionCreator = SDKActionCreator<SetPageNumberAction>;
export declare const setPageNumber: SetPageNumberActionCreator;
