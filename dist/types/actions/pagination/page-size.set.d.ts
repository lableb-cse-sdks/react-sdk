import { SDKActionCreator } from "../../types/actions";
export declare const SET_PAGE_SIZE = "SET_PAGE_SIZE";
export interface SetPageSizeAction {
    type: typeof SET_PAGE_SIZE;
    pageSize: number;
}
export declare type SetPageSizeActionCreator = SDKActionCreator<SetPageSizeAction>;
export declare const setPageSize: SetPageSizeActionCreator;
