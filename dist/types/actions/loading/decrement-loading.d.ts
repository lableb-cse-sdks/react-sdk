import { SDKActionCreator } from "../../types/actions";
export declare const DECREMENT_LOADING_STATE = "DECREMENT_LOADING_STATE";
export interface DecrementLoadingStateAction {
    type: typeof DECREMENT_LOADING_STATE;
}
export declare type DecrementLoadingStateActionCreator = SDKActionCreator<DecrementLoadingStateAction>;
export declare const decrementLoadingState: DecrementLoadingStateActionCreator;
