import { SDKActionCreator } from "../../types/actions";
export declare const INCREMENT_LOADING_STATE = "INCREMENT_LOADING_STATE";
export interface IncrementLoadingStateAction {
    type: typeof INCREMENT_LOADING_STATE;
}
export declare type IncrementLoadingStateActionCreator = SDKActionCreator<IncrementLoadingStateAction>;
export declare const incrementLoadingState: IncrementLoadingStateActionCreator;
