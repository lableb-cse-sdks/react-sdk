import { SDKActionCreator } from "../../types/actions";
import { Language } from "../../types/main";
export declare const SET_LANGUAGE = "SET_LANGUAGE";
export interface SetLanguageAction {
    type: typeof SET_LANGUAGE;
    language: Language;
}
export declare type SetLanguageActionCreator = SDKActionCreator<SetLanguageAction>;
export declare const setLanguage: SetLanguageActionCreator;
