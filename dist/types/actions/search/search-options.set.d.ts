import { LablebSearchOptions } from "@lableb/javascript-sdk";
import { SDKActionCreator } from "../../types/actions";
export declare const SET_SEARCH_OPTIONS = "SET_SEARCH_OPTIONS";
export interface SetSearchOptionsAction extends Partial<LablebSearchOptions> {
    type: typeof SET_SEARCH_OPTIONS;
}
export declare type SetSearchOptionsActionCreator = SDKActionCreator<SetSearchOptionsAction>;
export declare const setSearchOptions: SetSearchOptionsActionCreator;
