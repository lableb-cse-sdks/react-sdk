import { SDKActionCreator } from "../../types/actions";
export declare const RESET_FACET = "RESET_FACET";
export interface ResetFacetsAction {
    type: typeof RESET_FACET;
}
export declare type ResetFacetsActionCreator = SDKActionCreator<ResetFacetsAction>;
export declare const resetFacets: ResetFacetsActionCreator;
