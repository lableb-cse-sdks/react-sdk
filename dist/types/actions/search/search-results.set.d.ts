import { LablebClientSearchResponse } from '@lableb/javascript-sdk';
import { SDKActionCreator } from '../../types/actions';
export declare const SET_SEARCH_RESULTS = "SET_SEARCH_RESULTS";
export interface SetSearchResultsAction {
    type: typeof SET_SEARCH_RESULTS;
    searchResponse: LablebClientSearchResponse;
}
export declare type SetSearchResultsActionCreator = SDKActionCreator<SetSearchResultsAction>;
export declare const setSearchResults: SetSearchResultsActionCreator;
