import { SDKActionCreator } from "../../types/actions";
export declare const TOGGLE_FACET = "TOGGLE_FACET";
export interface ToggleFacetAction {
    type: typeof TOGGLE_FACET;
    facetName: string;
    facetValue: string;
}
export declare type ToggleFacetActionCreator = SDKActionCreator<ToggleFacetAction>;
export declare const toggleFacet: ToggleFacetActionCreator;
