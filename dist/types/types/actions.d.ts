export declare type SDKActionCreator<ACTION> = (action: Omit<ACTION, "type">) => ACTION;
