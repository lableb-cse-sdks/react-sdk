export interface BucketObject {
    value: string;
    count: number;
}
export declare type Language = 'en' | 'ar';
export interface LablebSearchFacets {
    [facetName: string]: {
        buckets: BucketObject[];
    };
}
