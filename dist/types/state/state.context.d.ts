/// <reference types="react" />
import { GlobalActions, GlobalState } from "./state.type";
export interface GlobalStateContextType {
    state: GlobalState;
    dispatch: (action: GlobalActions) => void;
}
export declare const GLOBAL_STATE_DEFAULT: GlobalState;
export declare const GlobalStateContext: import("react").Context<GlobalStateContextType>;
