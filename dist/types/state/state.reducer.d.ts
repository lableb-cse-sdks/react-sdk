import { GlobalActions, GlobalState } from "./state.type";
export declare const globalStateReducer: (state?: GlobalState | undefined, action: GlobalActions) => GlobalState;
