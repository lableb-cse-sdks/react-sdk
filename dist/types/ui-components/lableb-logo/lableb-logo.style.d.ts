export declare const useLablebLogoStyles: (data?: {
    theme?: Jss.Theme | undefined;
} | undefined) => import("jss").Classes<"logoAnchor" | "logo" | "poweredByContainer">;
