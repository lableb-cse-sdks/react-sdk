import { ReactNode } from "react";
export declare function SearchResultsCardsContainer({ children, shrink, }: {
    shrink?: boolean;
    children: ReactNode;
}): JSX.Element;
