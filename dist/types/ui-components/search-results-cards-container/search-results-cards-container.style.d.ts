export declare const useSearchResultsCardsContainer: (data?: {
    theme?: Jss.Theme | undefined;
} | undefined) => import("jss").Classes<"shrinkContainer" | "searchResultsContainer">;
