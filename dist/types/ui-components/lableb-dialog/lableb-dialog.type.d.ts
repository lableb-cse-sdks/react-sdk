import { ReactNode } from "react";
import { Language } from "../../types/main";
export interface LablebDialogProps {
    header: ReactNode;
    body: ReactNode;
    open: boolean;
    onClose: () => void;
    hideContainers?: boolean;
    stretchContent?: boolean;
    showAcceptButton?: boolean;
    onAccept?: () => void;
    language: Language;
}
