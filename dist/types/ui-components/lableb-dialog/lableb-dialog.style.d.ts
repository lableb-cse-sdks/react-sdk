export declare const useLablebDialogStyles: (data?: {
    theme?: Jss.Theme | undefined;
} | undefined) => import("jss").Classes<"dialog" | "stretchContent" | "hiddenDisplay" | "dialogRoot" | "dialogHeader" | "dialogBody" | "closeIcon" | "dialogActions" | "dialogCloseButton" | "dialogAcceptButton" | "fadeIn" | "fadeOut">;
