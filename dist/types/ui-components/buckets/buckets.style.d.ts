export declare const useBucketsStyles: (data?: {
    theme?: Jss.Theme | undefined;
} | undefined) => import("jss").Classes<"facetValuesContainer" | "facetValuesContainerColumn" | "moreFacetsIcon" | "lessFacetsIcon" | "expandFacetsValuesBtnContainer" | "flipChevron" | "expandFacetsValuesBtnContainerStartAlign">;
