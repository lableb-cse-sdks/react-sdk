import { BucketObject, Language } from '../../types/main';
export interface BucketsProps {
    buckets: BucketObject[];
    language: Language;
    useButtonForMoreBuckets?: boolean;
    singleColumn?: boolean;
    onBucketClick: (bucket: BucketObject) => void;
    shrinkSize?: number;
    includeCount?: boolean;
    useCheckbox?: boolean;
    selectedBuckets: string[];
}
