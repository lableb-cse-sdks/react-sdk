export declare const useShowMoreFacetsStyles: (data?: {
    theme?: Jss.Theme | undefined;
} | undefined) => import("jss").Classes<"expandMoreContainer" | "expandMoreButton" | "reverseChevronBottom">;
