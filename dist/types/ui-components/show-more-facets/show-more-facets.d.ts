/// <reference types="react" />
import { ShowMoreFacetsButtonProps } from './show-more-facets.type';
export declare function ShowMoreFacetsButton(props: ShowMoreFacetsButtonProps): JSX.Element;
