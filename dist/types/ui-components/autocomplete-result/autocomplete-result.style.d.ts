export declare const useAutocompleteResultStyles: (data?: {
    theme?: Jss.Theme | undefined;
} | undefined) => import("jss").Classes<"autocompleteSingleResult">;
