/// <reference types="react" />
import { AutocompleteResultProps } from './autocomplete-result.type';
export declare function AutocompleteResult(props: AutocompleteResultProps): JSX.Element;
