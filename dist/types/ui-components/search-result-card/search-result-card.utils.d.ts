import { ReactNode } from "react";
export declare function loadImageElement({ imageURL, timeoutT, imageProps, placeholderImage, }: {
    imageURL: string;
    timeoutT?: number;
    imageProps: any;
    placeholderImage: ReactNode;
}): Promise<unknown>;
