/// <reference types="react" />
import { LablebDocumentWithFeedback, LablebDocumentWithRecommendFeedback } from "@lableb/javascript-sdk";
import { Language } from "../../types/main";
export declare function SearchResultCard({ document, onClick, language, hideRelatedButton, onRelatedClick, shrink, }: {
    document: LablebDocumentWithFeedback | LablebDocumentWithRecommendFeedback;
    onClick: (document: LablebDocumentWithFeedback | LablebDocumentWithRecommendFeedback) => void;
    onRelatedClick?: (document: LablebDocumentWithFeedback | LablebDocumentWithRecommendFeedback) => void;
    language: Language;
    hideRelatedButton?: boolean;
    shrink?: boolean;
}): JSX.Element;
