export declare const useSearchResultCardStyles: (data?: {
    theme?: Jss.Theme | undefined;
} | undefined) => import("jss").Classes<"title" | "image" | "container" | "shrinkContainer" | "placeholderImage" | "loadingImage" | "description" | "relatedContainer" | "price" | "relatedButton">;
