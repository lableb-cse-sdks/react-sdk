export declare const useSearchIconStyles: (data?: {
    theme?: Jss.Theme | undefined;
} | undefined) => import("jss").Classes<"searchIcon" | "searchIconRTL">;
