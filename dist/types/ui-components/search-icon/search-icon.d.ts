/// <reference types="react" />
import { SearchIconProps } from './search-icon.type';
export declare function SearchIcon(props: SearchIconProps): JSX.Element;
