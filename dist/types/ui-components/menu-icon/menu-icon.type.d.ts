import { Language } from '../../types/main';
export interface MenuIconProps {
    language: Language;
    onClick: () => void;
    selectedFacetsCount: number;
}
