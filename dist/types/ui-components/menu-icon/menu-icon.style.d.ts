export declare const useMenuIconStyles: (data?: {
    theme?: Jss.Theme | undefined;
} | undefined) => import("jss").Classes<"count" | "menuIcon" | "menuIconContainer">;
