/// <reference types="react" />
import { MenuIconProps } from './menu-icon.type';
export declare function MenuIcon(props: MenuIconProps): JSX.Element;
