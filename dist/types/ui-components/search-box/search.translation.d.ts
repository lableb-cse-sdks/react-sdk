export declare const translations: {
    ar: {
        INPUT_PLACEHOLDER: string;
        SHOW_MORE: string;
        SHOW_LESS: string;
        SEARCH: string;
        LABLEB_CLOUD_SEARCH: string;
        SEARCH_FILTERS: string;
        OPTIONS: string;
        POWERED_BY: string;
        FILTERS: string;
        RESET: string;
        RELATED: string;
        CLOSE: string;
        ACCEPT: string;
        RECOMMENDATIONS: string;
    };
    en: {
        INPUT_PLACEHOLDER: string;
        SHOW_MORE: string;
        SHOW_LESS: string;
        SEARCH: string;
        LABLEB_CLOUD_SEARCH: string;
        SEARCH_FILTERS: string;
        OPTIONS: string;
        POWERED_BY: string;
        FILTERS: string;
        RESET: string;
        RELATED: string;
        CLOSE: string;
        ACCEPT: string;
        RECOMMENDATIONS: string;
    };
};
