import { LablebDocumentWithFeedback, SelectedFacets } from '@lableb/javascript-sdk';
import { LablebSearchFacets, Language } from '../../types/main';
export interface SearchBoxProps {
    facets: LablebSearchFacets;
    language: Language;
    inlineFilters: boolean;
    hasInlineLogo: boolean;
    autocompleteResults: LablebDocumentWithFeedback[];
    value: string;
    onChange: (value: string) => void;
    selectedFacets: SelectedFacets;
    onFacetToggle: (params: {
        facetName: string;
        facetValue: string;
    }) => void;
    onAutocompleteResultClick: (document: LablebDocumentWithFeedback) => void;
    loading?: boolean;
    openAutocompleteResults: boolean;
    setOpenAutocompleteResults: (value: boolean) => void;
}
