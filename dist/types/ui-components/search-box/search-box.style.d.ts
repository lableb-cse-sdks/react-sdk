export declare const useSearchBoxStyles: (data?: {
    theme?: Jss.Theme | undefined;
} | undefined) => import("jss").Classes<"hr" | "input" | "container" | "root" | "containerWithAutocompleteResults" | "autocompleteResultsContainer" | "searchFiltersContainer" | "loadingLine" | "loadingLineFadeOut::after" | "loadingLineFadeOut">;
