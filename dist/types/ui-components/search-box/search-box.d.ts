/// <reference types="react" />
import { SearchBoxProps } from './search-box.type';
export declare function SearchBox(props: SearchBoxProps): JSX.Element;
