/// <reference types="react" />
import { SingleBucketProps } from './single-bucket.type';
export declare function SingleBucket(props: SingleBucketProps): JSX.Element;
