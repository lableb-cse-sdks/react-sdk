export declare const useSingleBucketStyles: (data?: {
    theme?: Jss.Theme | undefined;
} | undefined) => import("jss").Classes<"facetValue" | "singleFacetValueContainer" | "singleFacetValueContainerActive" | "facetCount" | "plusIcon" | "removeIcon" | "checkboxContainer" | "checkboxInput" | "checkboxLabel" | "checkboxLabelWithText" | "flexContainer">;
