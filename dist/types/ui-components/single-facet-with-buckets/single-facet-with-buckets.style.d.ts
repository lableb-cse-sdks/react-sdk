export declare const useSingleFacetWithBucketsStyles: (data?: {
    theme?: Jss.Theme | undefined;
} | undefined) => import("jss").Classes<"facetName" | "facetItem" | "facetNameContainer" | "bucketSize">;
