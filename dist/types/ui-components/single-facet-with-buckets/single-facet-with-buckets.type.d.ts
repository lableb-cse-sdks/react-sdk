import { BucketObject, Language } from "../../types/main";
export interface SingleFacetWithBucketsProps {
    facetName: string;
    onBucketClick: (bucket: BucketObject) => void;
    language: Language;
    buckets: BucketObject[];
    includeBucketsSize?: boolean;
    useButtonForMoreBuckets?: boolean;
    singleColumn?: boolean;
    includeCount?: boolean;
    bucketsSize?: number;
    useCheckbox?: boolean;
    selectedBuckets: string[];
}
