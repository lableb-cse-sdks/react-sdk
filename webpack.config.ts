import { Configuration } from 'webpack';
import path from 'path';

const reactConfigurations: Configuration = {
    mode: process.env.MODE == 'development' ? 'development' : 'production',
    entry: path.join(__dirname, 'src/index.ts'),
    output: {
        path: path.join(__dirname, 'dist'),
        filename: 'index.js',
        globalObject: 'this',
        library: 'ReactSDK',
        libraryTarget: 'umd',
        umdNamedDefine: true,
    },
    target: 'node',
    devtool: process.env.MODE == 'development' ? 'source-map' : undefined,
    resolve: {
        extensions: ['.ts', '.tsx', '.module.css', '.js', '.css'],
        alias: {},
    },
    module: {
        rules: [
            {
                test: /\.(ts|tsx)$/,
                use: 'ts-loader',
                include: path.join(__dirname, 'src'),
                exclude: /node_modules/
            },
            {
                test: /\.(png|svg|jpg|gif)$/,
                type: process.env.MODE == 'development' ? 'asset/inline' : 'asset/inline',
            },
        ]
    },
    externals: {
        "@lableb/javascript-sdk": "@lableb/javascript-sdk",
        "react": "react",
        "react-dom": "react-dom",
        "immer": "immer",
        "lodash": "lodash",
        "lodash-es": "lodash-es",
    }
}


export default reactConfigurations;