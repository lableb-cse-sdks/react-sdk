import logo from './logo.svg';
import './App.css';
import { Composition } from 'atomic-layout';
import {
  LablebProvider,
  LablebSearch,
  LablebSearchResults,
  LablebSearchFilters,
  LablebRecommend,
  VirtualSearchFilters,
  LablebPagination
} from '@lableb/react-sdk';




function AppRenderProps() {

  return (
    <div dir="LTR">

      <LablebProvider
        platformOptions={{
          platformName: 'platformName',
          APIKey: 'APIKey',
        }}

        layoutDirection="LTR"
      >


        <Composition
          height={'90vh'}
          templateRows={'auto 1fr auto'}
          justifyItems="center"
          gap={20}
          padding={20}
        >

          <LablebSearch preventAutoSearch>
            {
              ({
                query,
                setQuery,
                autocompleteResults,
                loading,
                searchAtLableb,
                openAutocompleteResults,
                sendAutocompleteFeedback,
                setOpenAutocompleteResults,
              }) => (
                <div>

                  <div>
                    <input
                      placeholder="search"
                      value={query}
                      onChange={event => setQuery(event.target.value)}
                    />

                    <button onClick={searchAtLableb}>
                      {`search${loading ? 'ing' : ''}`}
                    </button>

                  </div>

                  <div>
                    {
                      autocompleteResults.map(doc => (
                        <p onClick={() => sendAutocompleteFeedback(doc)}>
                          {doc?.phrase}
                        </p>
                      ))
                    }
                  </div>


                </div>
              )
            }
          </LablebSearch>


          <Composition
            justify="stretch"
            templateCols={'1fr 3fr'}
          >

            <LablebSearchFilters>
              {
                ({
                  facets,
                  resetFacets,
                  selectedFacets,
                  toggleFacet,
                }) => (
                  <div>

                    <div>
                      <h1>{'Filters'}</h1>
                      <button onClick={resetFacets}>{'reset filters'}</button>
                    </div>

                    <div>
                      {
                        Object.keys(facets)
                          ?.map(facetName => (
                            <div>
                              <p>{facetName}</p>
                              <div>
                                {
                                  facets[facetName].buckets.map((bucket, index) => (
                                    <div key={`${bucket.value}-${index}`}>
                                      <label htmlFor={`${bucket.value}-${index}`}>
                                        {bucket.value}
                                      </label>
                                      <input
                                        type="checkbox"
                                        checked={selectedFacets?.[facetName]?.includes(bucket.value)}
                                        onChange={(event) => toggleFacet({ facetName, facetValue: bucket.value })}
                                      />
                                    </div>
                                  ))
                                }
                              </div>
                            </div>
                          ))
                      }
                    </div>

                  </div>
                )
              }
            </LablebSearchFilters>

            <LablebSearchResults
              redirectToResultOnClick
              showRelatedDocuments
            >
              {
                ({
                  onSearchResultClick,
                  searchResults,
                }) => (
                  <>
                    <div style={{ display: 'flex', flexDirection: 'column' }}>
                      {
                        searchResults.map(doc => (
                          <div
                            style={{ border: '1px solid black' }}
                            key={doc.id} onClick={() => onSearchResultClick(doc)}>
                            <p>{doc.title}</p>
                          </div>
                        ))
                      }
                    </div>
                  </>
                )
              }
            </LablebSearchResults>


            {/* <LablebRecommend id="570-ar" /> */}
          </Composition>

          <div>
            <LablebPagination>
              {
                ({
                  currentPage,
                  hasNextPage,
                  hasNextTenPages,
                  hasPrevPage,
                  hasPrevTenPages,
                  onNextPage,
                  onNextTenPages,
                  onPrevPage,
                  onPrevTenPages,
                  pagesCount,
                  pagesNumbersToDisplay,
                  setPageNumber
                }) => (
                  <>

                    {
                      hasPrevTenPages ?
                        <button
                          onClick={onPrevTenPages}
                        >
                          {'prev 10 pages'}
                        </button>
                        :
                        null
                    }


                    {
                      hasPrevPage ?
                        <button
                          onClick={onPrevPage}>
                          {'prev page'}
                        </button>
                        :
                        null
                    }


                    {
                      pagesNumbersToDisplay
                        .map(page =>
                          <button
                            key={page}
                            style={{ color: page == currentPage ? 'red' : 'black' }}
                            onClick={() => setPageNumber(page)}
                          >
                            {page + 1}
                          </button>)
                    }



                    {
                      hasNextPage ?
                        <button
                          onClick={onNextPage}>
                          {'next page'}
                        </button>
                        :
                        null
                    }

                    {
                      hasNextTenPages ?
                        <button
                          onClick={onNextTenPages}>
                          {'next ten pages'}
                        </button>
                        :
                        null
                    }
                  </>
                )
              }
            </LablebPagination>
          </div>

        </Composition>


        <LablebRecommend
          id="542-en"
          recommendOptions={{
            limit: 2
          }}
        >
          {
            ({ recommendResults, sendRecommendFeedback }) => (
              <div>
                {
                  recommendResults.map(doc => (<div
                    onClick={() => sendRecommendFeedback(doc)}
                    style={{ border: '1px solid green' }}>

                    <p>{doc.id} {doc.title}</p>

                  </div>))
                }
              </div>
            )
          }
        </LablebRecommend>


      </LablebProvider>

    </div>
  );
}


export default AppRenderProps;
