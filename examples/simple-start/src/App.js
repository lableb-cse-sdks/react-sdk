import { Composition } from 'atomic-layout';
import {
  LablebProvider,
  LablebSearch,
  LablebSearchResults,
  LablebSearchFilters,
  LablebPagination,
} from '@lableb/react-sdk';


function App() {

  return (
    <div dir="LTR">

      <LablebProvider
        platformOptions={{
          platformName: 'platformName',
          APIKey: 'APIKey',
          indexName: 'indexName',
        }}

        layoutDirection="LTR"
      >


        <Composition
          height={'90vh'}
          templateRows={'auto 1fr auto'}
          justifyItems="center"
          gap={20}
          padding={20}
        >

          <LablebSearch
            inlineFilters
          />


          <Composition
            justify="stretch"
            templateCols={'1fr 3fr'}
          >
            <LablebSearchFilters

            />

            <LablebSearchResults
              openResultOnClick
            />


            {/* <LablebRecommend id="570-ar" /> */}
          </Composition>

          <div>
            <LablebPagination
              pageSize={9}
            />
          </div>

        </Composition>

      </LablebProvider>

    </div>
  );
}

export default App;



