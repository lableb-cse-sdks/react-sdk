import type { Config } from '@jest/types';
require('dotenv').config({ path: './.env.test' });

const jestConfig: Config.InitialOptions = {
    testTimeout: 90000,
    testEnvironment: 'jsdom',
    setupFilesAfterEnv: ['./jest.setup.ts'],
    moduleNameMapper: {
        "\\.(jpg|ico|jpeg|png|gif|eot|otf|webp|svg|ttf|woff|woff2|mp4|webm|wav|mp3|m4a|aac|oga)$": "<rootDir>/mocks/file-mock.js",
        "\\.(css|less)$": "<rootDir>/mocks/file-mock.js"
    }
}

module.exports = jestConfig;