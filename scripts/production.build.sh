#!/bin/bash

rm -rf dist
npx webpack
mv dist/src dist/types
rm -rf dist/mocks
rm -rf dist/*.d.ts
rm -rf dist/*.txt